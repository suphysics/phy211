---
layout: page
title: Homework 
category: top
permalink: homework.html
use_math: true
---

<a href="homework/hw5/hw5.pdf">Homework 5</a> is due Friday, March 7. No submissions will be accepted over email; if you are leaving for spring break early, 
you should submit it to your TA before you leave.

<a href="homework/hw4/hw4.pdf">Homework 4</a> is due Friday, February 21.

<a href="homework/hw3/homework3.pdf">Homework 3</a> is due Wednesday, February 12.

<a href="homework/hw2/hw2.pdf">Homework 2</a> is due in recitation on Friday, January 31.

<a href="homework/hw1/hw1.pdf">Homework 1</a> is due in recitation on Friday, January 24.

Homework 0 is the <a href="https://forms.gle/22a7vqDbUsRDVTKj8">mathematics pre-assessment</a>. It is due Sunday, January 19.
