---
layout: page
title: Calendar
permalink: calendarplan.html
use_math: true
---


This calendar is tentative and will likely change as the semester progresses. Exam dates will not change without overwhelming consensus, but topics might be adjusted if the calendar changes.

Note that the textbook does not track our presentation exactly; I've chosen the order of topics to simplify things for you, but that makes it a bit tougher to follow along in the book.



|-------------+------------------------------------------------------------------------------------------------------------------+--------------------------------+
| Class Date  | Topics                                                                                                           | Textbook sections              |
|:-----------:|:----------------------------------------------------------------------------------------------------------------:|:------------------------------:|
| 16 January  | Introduction and units   
| 18 January  | Equations of motion; calculus    
| 23 January  | Motion in one dimension         
| 25 January  | Vectors                        
| 30 January  | 2D kinematics                 
| 1  February | Review for Exam 1            
| 6  February | **Exam 1**                  
| 8  February | Newton's laws of motion     
| 13 February | Friction; forces in 2D     
| 15 February | Coupled objects           
| 20 February | Circular motion          
| 22 February | Universal gravitation; review for 
| 27 February | **Exam 2**             
| 29 February | Conservation of momentum (1D)  
| 5  March    | Conservation of momentum (2D) 
| 7  March    | The work-energy theorem
| 12+14 March | **SPRING BREAK**     
| 19 March    | Elasticity; potential energy
| 21 March    | Combining momentum and energy
| 26 March    | Power
| 28 March    | Review for Exam 3
| 2  April    | **Exam 3**                
| 4  April    | Measuring rotation 
| 9  April    | Torque and static equilibrium
| 11 April    | Rotational dynamics
| 16 April    | Combining rotation and translation; rolling
| 18 April    | Rotational kinetic energy
| 23 April    | Angular momentum
| 25 April    | Review for Exam 4; course closing
| 8 May       | **FINAL EXAM**: 3PM-5PM Syracuse time       
|-------------+------------------------------------------------------------------------------------------------------------------+--------------------------------+

