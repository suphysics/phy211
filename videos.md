---
layout: page
title: Videos and Solutions
category: top
permalink: videos.html
use_math: true
---


Here you can find video explanations of our homework and recitation exercises, as well as written solutions
to some things.

### Exam Solutions

* <a href="exam1-solutions.pdf">Written solutions to Exam 1</a> (by Walter)

### Practice Exam Explanations
* <a href="https://www.youtube.com/playlist?list=PLfe92KWZipRM3PAogVKSCFFqA5VojNtb4">Explanations for Practice Exam 1</a> (by Walter)


### Homework solutions

* Homework 1 videos: <a href="https://youtu.be/a4iuhUOjMh0">[Part 1]</a> and <a href="https://youtu.be/OJFHHr7fnRU">[Part 2]</a> (both by Sydney)
* <a href="homework/hw1/homework-1-solutions-2025.pdf">Homework 1 written solutions</a> (by Walter)
* Homework 2 videos: <a href="https://youtu.be/3HmHwv8qXGU">[Part 1]</a> and <a href="https://youtu.be/q9dFd_zt0K8">[Part 2]</a> (both by Sydney)
* <a href="homework/hw2/hw2-solutions.pdf">Homework 2 written solutions</a> (by Sydney)
* <a href="https://youtu.be/md-ueG4jeMI">Homework 3 videos</a>, by Sydney, guest starring other aerospace engineering students
* <a href="https://www.youtube.com/playlist?list=PLfe92KWZipRNsDAjRsNMICtDWIzUPLhQV">Homework 4 videos</a>, by Walter

### Recitation explanations

Note that these will take different forms based on the materials we have available. Some formats you might see here are:

* The written-out solutions that some of our instructors may make for themselves; these are often terse (since they are written by people who already understand the physics for their own use), but give an example of the ultimate simplicity of the calculations that you will need to do
* The instructor guides that I write for the TA's and coaches to use as reference. These are often verbose and contain a lot of discussion of "what we hope you will learn", and are written for an audience of TA's and coaches rather than students. However, I can post them here after the recitation in case you want to look at them.
* Video explanations of the solutions to things, often made by Sydney Jud, one of our coaches.

* Week 1 Day 1: <a href="recitation/week1/recitation-units-motion-solutions.pdf">Sydney's solutions on math with SI units</a>
* Week 1 Day 2: <a href="recitation/week1/recitation-1D-motion-1-solutions.pdf">Sydney's solutions on 1D motion (part I)</a>
* Week 2 Day 1: <a href="recitation/week2/recitation-1D-motion-2-teachers-guide.pdf">Instructor guide on 1D motion (part II)</a>
* Week 2 Day 2: <a href="recitation/week2/recitation-vectors-solution.pdf">Sydney's solutions to the recitation on vectors</a>
* Week 3 Day 1: <a href="recitation/week3/recitation-2D-motion-teachers-guide.pdf">Instructor guide on 2D motion</a>; <a href="recitation/week3/recitation-2D-motion-solutions.pdf">Sydney's solutions</a>
* Week 4 Day 2: <a href="recitation/week4/recitation-forces-solution.pdf">Sydney's solutions to the recitation on forces</a>; <a href="recitation/week4/recitation-forces-withguide.pdf">instructor guide to the recitation on forces</a>
* Week 5 Day 2: <a href="recitation/week5/recitation-uniform-circular-motion-solutions.pdf">Sydney's solutions to the recitation on circular motion</a>

### Recitation explanation videos

Sydney has a YouTube channel where she posts video explanations of some of our recitations. You can find them here:

* <a href="https://www.youtube.com/watch?v=7Sj_Pt1HUsA&t=1616s">Week 1 Day 2: 1D motion, part 1</a> (by Sydney) 
* <a href="https://youtu.be/tkz12vy5tGc">Week 2 Day 1: 1D motion, part 2</a> (by Sydney) 
* <a href="https://youtu.be/6_-hPlO6yag">Week 2 Day 2: Vectors</a> (by Sydney)
* <a href="https://youtu.be/9GP2jFM7wn8">Week 3 Day 1: 2D Motion</a> (by Sydney)
* <a href="https://youtu.be/OpLJnEZ_njw">Week 4 Day 2: Forces in One Dimension</a> (by Sydney)
* <a href="https://youtu.be/NnG6hxB9tmM">Guide to Physical Interpretation</a> (by Sydney)
* <a href="https://youtu.be/2fzoGFiGe28">Week 5 Day 1: Forces in Two Dimensions and Coordinate Systems</a> (by Sydney)
* <a href="https://youtu.be/bp80Kwc15tI">Week 5 Day 2: Circular Motion</a> (by Sydney)

<!--
### Practice exam explanations

* <a href="practice-exam-3-solutions.pdf">Solutions to Practice Exam 3</a> (written, by Walter)

### General tutorials

* <a href="https://youtu.be/okCdIOfH6nw">How to Dot Product</a> (by Sydney)

 <a href="https://youtu.be/9GP2jFM7wn8">Week 3 Day 1: 2D motion</a> (by Sydney)

* <a href="https://youtu.be/2fzoGFiGe28">Week 5 Day 1: Forces in Two Dimensions and Coordinate Systems</a> (by Sydney)
* <a href="https://youtu.be/bp80Kwc15tI">Week 5 Day 2: Motion in a circle</a> (by Sydney)
* <a href="https://youtu.be/zenumTWNQ5k">Week 6 Day 1: Earth's Gravity, Orbits, and Apparent Weight</a> (by Sydney)

* <a href="https://youtu.be/eeYPxlfYPPE">Week 7 Day 2: The work-energy theorem</a> (by Sydney)
* <a href="https://youtu.be/rDhKZ2AE5dw">Week 8 Day 1: The work-energy theorem and potential energy</a> (by Sydney)
* <a href="https://youtu.be/DYAVKk4KH7c?si=NGe4tEDlRYH_Q6oF">Week 8 Day 2 and Week 9 Day 1 first problem</a> (by Sydney)
* <a href="https://youtu.be/dAeohBci-dc">Week 9 Day 2: the conservation of momentum</a> (by Sydney)

* <a href="https://youtu.be/7sv7DUZtgI8">Week 11 Day 2: calculating torques and static equilibrium</a> (by Sydney)
* <a href="recitation-week13-day1-solutions.pdf">Week 13 Day 1: combining rotation and translation</a> (by Walter)
* <a href="https://youtu.be/2g0O4Tw6VUU">Week 14 Day 1: transmissions and gear ratios</a> (by Sydney)
### Homework explanations

### Other
* <a href="hw5-solutions.pdf">Written homework 5 solutions</a> (by Walter)
* <a href="hw6-solutions.pdf">Written homework 6 solutions</a> (by Walter)
* <a href="hw7-solutions.pdf">Written homework 7 solutions</a> (by Walter)
-->
