\documentclass[12pt]{article}
\setlength\parindent{0pt}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{enumitem}
\setlength{\parskip}{4mm}
\usepackage[left=2cm, right=2cm, top=1.5cm, bottom=1cm]{geometry}
%\usepackage[margin=0.5in, paperwidth=13.5in, paperheight=8.4375in]{geometry}
\def\LL{\left\langle}   % left angle bracket
\def\RR{\right\rangle}  % right angle bracket
\def\LP{\left(}         % left parenthesis
\def\RP{\right)}        % right parenthesis
\def\LB{\left\{}        % left curly bracket
\def\RB{\right\}}       % right curly bracket
\def\PAR#1#2{ {{\partial #1}\over{\partial #2}} }
\def\PARTWO#1#2{ {{\partial^2 #1}\over{\partial #2}^2} }
\def\PARTWOMIX#1#2#3{ {{\partial^2 #1}\over{\partial #2 \partial #3}} }
\newcommand{\BI}{\begin{itemize}}
\newcommand{\EI}{\end{itemize}}
\newcommand{\BE}{\begin{displaymath}}
\newcommand{\EE}{\end{displaymath}}
\newcommand{\BNE}{\begin{equation}}
\newcommand{\ENE}{\end{equation}}
\newcommand{\BEA}{\begin{eqnarray}}
\newcommand{\EEA}{\nonumber\end{eqnarray}}
\newcommand{\EL}{\nonumber\\}
\newcommand{\la}[1]{\label{#1}}
\newcommand{\ie}{{\em i.e.\ }}
\newcommand{\eg}{{\em e.\,g.\ }}
\newcommand{\cf}{cf.\ }
\newcommand{\etc}{etc.\ }
\newcommand{\Tr}{{\rm tr}}
\newcommand{\etal}{{\it et al.}}
\newcommand{\OL}[1]{\overline{#1}\ } % overline
\newcommand{\OLL}[1]{\overline{\overline{#1}}\ } % double overline
\newcommand{\OON}{\frac{1}{N}} % "one over N"
\newcommand{\OOX}[1]{\frac{1}{#1}} % "one over X"

\def\BS{\bigskip}

\begin{document}
\pagenumbering{gobble}
\Large
\centerline{\sc{Recitation Exercises -- Vectors}}
\centerline{\color{red}Teachers' Guide}
\normalsize
\centerline{\sc{Week 2, Day 2}}


\rmfamily

\medskip

\rm In this recitation, you will practice:

\BI
\item Converting vectors from ``magnitude and direction'' representation to ``$x-$ and $y-$component'' representation
\item Breaking vectors into their $x-$ and $y-$ components
\item Representing vectors in different coordinate systems
\item Using vector addition and subtraction to solve problems
\EI

This recitation activity is absolutely vital for the rest of the semester. If you don't understand anything you have done today, please go visit the Physics Clinic to work with a tutor. If you are having trouble with the trigonometry here, please contact Walter at \url{wafreema@syr.edu} and he'll tell you about tutoring opportunities for trig.

{\color{red} We want to make sure students understand vectors graphically first, then figure out how to convert from ``magnitude and direction'' to ``x- and y-components'' -- where x and y may be any two perpendicular directions.}

\newpage
\begin{minipage}{0.4\textwidth}
{\bf Question 1.} The streets in Manhattan are laid out in a grid, but that grid is aligned with the island, rather than along the compass directions. Avenues run 28.9 degrees east of north, while streets run 28.9 degrees north of west. 

\BS

This means that there are two sensible coordinate systems in Manhattan:


\BI
\item North/South/East/West, aligned with the compass
\item Uptown/Downtown/Crosstown, aligned with the streets.
\EI
	(In this map and the next one, “up” is due north.)

\end{minipage}
\begin{minipage}{0.6\textwidth}
	\begin{center}
	
	
	\includegraphics[width=0.95\textwidth]{manhattan-1.png}
	

\end{center}
\end{minipage}
\BS\BS

The staff astronomer at the Natural History Museum walks from Penn Station to the Natural History Museum, going 3.3 km north along Eighth Avenue. How far east and how far north did she walk? ({\it Note: Her path carries her off of the top of the page. You will want to draw an arrow aligned with her path below, and then draw its components.})

{\color{red} Here they'll need to draw a vector below going up and to the right, making a $28.9^\circ$ angle with the vertical; this is the hypotenuse. The north and east ``components'' are the legs of that triangle. This gives:
	
	\begin{itemize}
		\item Amount east: 3.3 km $\sin 28.9^\circ$
		\item Amount north: 3.3 km $\cos 28.9^\circ$
	\end{itemize}
	
	A very easy self-check they should do: they should know cosine is bigger than sine for angles less than $45^\circ$. This means that the north component is bigger than the east component, which makes sense.


}
	


\newpage


{\bf Question 2.} Here is the displacement vector pointing from the Metropolitan Opera to Carnegie Hall.
	\BS
	
	
		\begin{minipage}{0.5\textwidth}
		
		\includegraphics[width=0.9\textwidth]{manhattan-2.png}
	\end{minipage}
	\begin{minipage}{0.49\textwidth}
		

	
	Draw on top of the picture:
	
	\BI
	\item its component in the direction of the Manhattan avenues;
	\item its component perpendicular to them;
	\item its component along the North-South axis;
	\item its component along the East-West axis.
	\EI

	
	It may be helpful to draw the first two on the bottom and left, and the second two on the top and right -- just so they don't overlap on your picture.
\end{minipage}

{\color{red}The biggest pitfall here is mixing the ``avenues and streets'' basis and the ``north-south and east-west'' basis. They'll draw one vector along the streets and then the other vector along the north/south direction. The arrow should be the hypotenuse of both triangles they draw.}
	

\newpage
{\bf Question 3.} A hiker in the forest walks 5 km due north and then 2 km due east, and then wants to return to their original spot by the shortest route possible. 

Draw their path. 

{\color{red}This should be a right triangle with legs 5 km and 2 km, with a hypotenuse closing the triangle.}






\vspace{3in}

Which direction should they walk, and for how far?

{\color{red} They will get tripped up here on talking about directions. They will need to go ``mostly south and a little bit west''. They can either draw a triangle, solve for one of the angles, and say ``this angle right here is $\tan^{-1} \frac{2}{5}$'', or they can say ``at an angle of $\tan^{-1} \frac{2}{5}$ west of south''.
	
	The distance is from the Pythagorean theorem, $\sqrt{5^2 + 2^2}$ km.
}








\newpage
{\bf Question 4.} Now our hiker walks 3 km due north, then 4 km at an angle 30 degrees south of east, and then finally 5 km at an angle 45 degrees north of west. They then wants to return to their starting point, as before. Which direction should they travel in, and for how far? 

Hint 1: It will help visualize things to draw the hiker's path first.

Hint 2: Remember that you can add vectors by converting them to $x-$ and $y-$components and then adding the components. The simplest approach here is:

\begin{enumerate}
	\item Convert the vectors to component form
	\item Add the components
	\item Convert the resulting sum back to the magnitude-and-direction form that you are looking for.
\end{enumerate}


{\color{red} Here the biggest trouble for students is either organization or getting distracted by their high school geometry class. High school geometry classes in the US love to present students with complicated blobs of triangles that they can ``solve'' by various geometric rules (especially the law of sines -- they love the law of sines). 
	
	{\bf Do not let the students do this.} It is way harder than it needs to be. Just add up the x- and y- components.}

\newpage
{\bf Question 5.} A swimmer can swim 5 km/hr in still water. They want to swim directly across a river. However, there is a current in the river, with a speed of 2 km/hr. If they swim directly across, they will drift downstream due to the current. Thus, in order to get where they want to go, they needs to angle themselves upstream.

\begin{enumerate}
\item There are three interesting vectors in this problem: the velocity of the current, the swimmer's velocity relative to the current, and their velocity relative to the shore. How do they relate? State this both mathematically (for instance, ``this vector plus that vector equals this other vector''), and geometrically (draw a picture).


{\color{red}

Here students will often mess this up by jumping straight into doing arithmetic before thinking, visualizing, and drawing.

In this case, the velocity vector of the current (water relative to shore) plus the velocity vector of the swimmer relative to the water equals the velocity of the swimmer relative to shore. If we like equations we could do the following:

$$\vec v_{\text{swimmer relative to shore}} = \vec v_{\text{swimmer relative to water}} + \vec v_{\text{water relative to shore}}$$

We know the swimmer wants their velocity relative to the shore to be directly across. This means that they must point their body upstream a little bit so that the ``upstream component'' of their velocity relative to the water cancels the ``downstream component'' of the current, resulting in a total vector that is straight across.
	
}



\item At what angle must they try to swim in order to proceed directly across the river?

{\color{red}
Obviously we need to do some trigonometry. But the students who skipped the ``think carefully before drawing stuff'' in their triangle will have mislabeled it. In this case the ``5 km/hr'' swimming velocity vector is the {\it hypotenuse}; we must add one of the legs (the ''2 km/hr'' current vector) to that to get the ``straight across'' total velocity vector.

So the correct trig here is at an angle $\theta = \sin^{-1} \frac{2}{5}$ upstream -- not $\tan^{-1}$, which is what you get if you don't think about which side of the triangle is which.
}



\item If the river is 200 m across, how long will it take them to cross?

{\color{red}

More triangle math -- we have to figure out the velocity they actually have when crossing. A little thought should reveal that it's a bit {\it less} than 5 km/hr since they have to fight the water a bit.

The Pythagorean theorem gives $2^2 + v_{\text{total}}^2 = 5^2$, so the resulting velocity is $\sqrt{21}$ km/hr. 

}

\end{enumerate}

\end{document}



