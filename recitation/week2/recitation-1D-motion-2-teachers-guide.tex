\documentclass[12pt]{article}
\setlength\parindent{0pt}
\usepackage{xcolor}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{graphicx}
\setlength{\parskip}{4mm}
\usepackage[left=2cm, right=2cm, top=1.5cm, bottom=1cm]{geometry}
\def\LL{\left\langle}   % left angle bracket
\def\RR{\right\rangle}  % right angle bracket
\def\LP{\left(}         % left parenthesis
\def\RP{\right)}        % right parenthesis
\def\LB{\left\{}        % left curly bracket
\def\RB{\right\}}       % right curly bracket
\def\PAR#1#2{ {{\partial #1}\over{\partial #2}} }
\def\PARTWO#1#2{ {{\partial^2 #1}\over{\partial #2}^2} }
\def\PARTWOMIX#1#2#3{ {{\partial^2 #1}\over{\partial #2 \partial #3}} }
\newcommand{\BI}{\begin{itemize}}
\newcommand{\EI}{\end{itemize}}
\newcommand{\BE}{\begin{displaymath}}
\newcommand{\EE}{\end{displaymath}}
\newcommand{\BNE}{\begin{equation}}
\newcommand{\ENE}{\end{equation}}
\newcommand{\BEA}{\begin{eqnarray}}
\newcommand{\EEA}{\nonumber\end{eqnarray}}
\newcommand{\EL}{\nonumber\\}
\newcommand{\la}[1]{\label{#1}}
\newcommand{\ie}{{\em i.e.\ }}
\newcommand{\eg}{{\em e.\,g.\ }}
\newcommand{\cf}{cf.\ }
\newcommand{\etc}{etc.\ }
\newcommand{\Tr}{{\rm tr}}
\newcommand{\etal}{{\it et al.}}
\newcommand{\OL}[1]{\overline{#1}\ } % overline
\newcommand{\OLL}[1]{\overline{\overline{#1}}\ } % double overline
\newcommand{\OON}{\frac{1}{N}} % "one over N"
\newcommand{\OOX}[1]{\frac{1}{#1}} % "one over X"

\def\BS{\bigskip}

\begin{document}
\pagenumbering{gobble}
\Large
\centerline{\sc{Recitation Exercises -- 1D Motion (part 2)}}
\normalsize
\centerline{\sc{Week 2, Day 1}}

\begin{center}
	\color{red} \Large Instructor's guide
\end{center}


\medskip

\rm In this recitation, you will practice:

\BI
\item Translating back and forth between words and mathematics as a way to describe motion
\item Solving problems symbolically rather than numerically
\item Dealing with situations with a changing acceleration (piecewise constant)
\item Relating position, velocity, and acceleration graphically
\EI

{\color{red}Here we want them to do the ``braking car problem'' -- and then do it {\it again} without any numbers. 
	
	Then we'll move on to piecewise constant acceleration and analyze it both graphically and numerically.
}

\newpage
%
%
%\centerline{\Large Question 1: a braking car}
%\begin{center}
%	\bf (If your group didn't finish this problem last week, do it here.) 
%\end{center}
%
%A car is traveling at 30 m/s and applies its brakes to slow down to 10 m/s. If it is able to decelerate at 5 $\rm m/\rm s^2$, how far does it travel during the braking period?
%
%\begin{enumerate}
%	\item Write expressions for the car's position and velocity as a function of time. What moment makes sense to choose as your reference time $t=0$?
%	
%
%	{\color{red} Here, they need to interpret the word ``decelerate'' to mean ``accelerate in a direction opposite its motion'' -- i.e., if $v_0$ is positive, $a$ is negative. The moment when the car first applies its brakes is the obvious choice for $t=0$ (the moment when the acceleration begins).}
%	
%	\item How can you translate the question ``How far does it travel during the braking period?'' into a sentence about your algebraic variables? Again, fill in the blanks: 
%	
%	\begin{center}
%		{\bf ``What is the value of \underline{\hspace{0.7in}} at the time when \underline{\hspace{0.7in}} is equal to \underline{\hspace{0.7in}}?''} 
%	\end{center}
%	{\color{red} Here, again, we are practicing this while it is easy; it will get harder later. Assuming $x=0$ is also the position when the car applies its brakes, we want ``What is the value of position at the time that $v=10$ m/s?''}
%	
%	\item What intermediate quantity must you find before you find the distance traveled? Following the above recipe you created for yourself, find it.
%	
%	{\color{red} This is the time. They'll need to go to the velocity equation to find that the braking time is 4 seconds.}
%	
%	
%
%	
%	
%	\item Finally, how far does the car travel during the braking period?
%	
%	{\color{red}Here they should recognize that they their sentence above -- ``What is the value of position at the time that $v=10$ m/s?'' -- gives them a roadmap for what to do: now that they have found the time that $v=10$ m/s, they should find the position at that time by substituting this time into their position equation.
%		
%		Their position equation is 
%		
%		$$x(t) = v_0 t + \frac{1}{2}at^2$$
%		
%		Substituting in numbers we get:
%		
%		$$x(t) = (30\, \rm m/\rm s) (4 s) - \frac{1}{2}(-5\, \rm m/\rm s^2)(4 s)^2 = 120 m - 40 m = 80 m$$}
%	
%\end{enumerate}
%
%\newpage

\centerline{\Large Question 1: the braking car, with variables}

\begin{center}
	\bf (This is the same as the exercise you did last week, but here you'll find the answer in terms of variables, working symbolically rather than numerically. This is often a clearer and more straightforward way to do physics once you get the hang of it.) 
\end{center}

{\color{red}Students, particularly weaker ones, are often resistant to this approach, even though it makes the actual physics much clearer. This is in part because sometimes they don't know how to do algebra. {\bf This problem is a great algebra litmus test.} Be especially on the lookout for students who can't do the algebra here and help them with any gaps they have.

This question is also a sneaky backdoor derivation of 

$$v_f^2 - v_0^2 = 2a\Delta x$$

which is a thing many will know from high school, but won't realize is not a new thing but a consequence of things they already know.  It is also intimately related to the work-energy theorem, as we'll see later.
}

A car is traveling at a speed $v_0$ and applies its brakes to slow down to a speed $v_f$. If it is able to decelerate at an acceleration $a_b$, how far does it travel during the braking period?

\begin{enumerate}
\item Write expressions for the car's position and velocity as a function of time, as before.

{\color{red}
	
	\begin{align}
		x(t) &= v_0 t - \frac{1}{2}a_bt^2   \\
		v(t) &= v_0 - a_b t
	\end{align}
	
	where we've taken $a_b$ to be a magnitude, so $a = - a_b$.
}

\item Using the same process that you did in the previous problem, find an expression that tells how far the car travels during the braking period. This should depend only on $v_0$, $v_f$, and $a_b$ -- the physical parameters of the problem. There are a few algebra skills you might need here; if your group needs assistance with this, please ask your instructors!

{\color{red}
	
	In gory step-by-step detail:
	
	First they will need to find the time it takes to reach $v_f$ from equation (2):
	
	$$t = \frac{v_0 - v_f}{a_b}$$
	
	Then they substitute that time into equation (1):
	
	$$x(t) = v_0 \frac{v_0 - v_f}{a_b} - \frac{1}{2}a_b \left(\frac{v_0 - v_f}{a_b}\right)^2$$
	
	If they are getting afraid of algebra at this point, reassure them that they should just keep going and things will cancel. 
	
	First they'll need to recognize that a squared fraction is the same as squaring top and bottom:
	
	
	$$x(t) = v_0 \frac{v_0 - v_f}{a_b} - \frac{1}{2}a_b \frac{(v_0 - v_f)^2}{a_b^2}$$
	
	Then they need to cancel a factor of $a_b$:
	
	$$x(t) = v_0 \frac{v_0 - v_f}{a_b} - \frac{1}{2} \frac{(v_0 - v_f)^2}{a_b}$$
	
	If it weren't for that factor of $\frac{1}{2}$, you could easily get a common denominator. Fix this by combining the 1/2 with the second term and then get a common denominator:
	
	
	$$x(t) = v_0 \frac{2v_0 - 2v_f}{2a_b} -  \frac{(v_0 - v_f)^2}{2a_b}$$
	

  Distributing the factor of $v_0$ in the first term (they'll need to know how this works too -- some won't) and expanding the numerator of the second term, we get:
  
  	$$x(t) = \frac{2v_0^2 - 2v_fv_0}{2a_b} -  \frac{(v_0^2 + v_f^2 - 2v_0 v_f)}{2a_b}$$
  
	Now we can combine the two fractions and cancel stuff:
	
	
	$$x(t) =  \frac{v_0^2 - v_f^2}{2a_b}$$
	
	which of course is equivalent to the ``third kinematics equation''.
	
	If they're leery of the algebra here, reassure them that this is about as bad as it gets in phy211.  There's this moment and then one more in HW4 with icky algebra. But they {\it do} need to know how to do this.
	
	
}

\end{enumerate}
\newpage

\centerline{\Large Question 2: a rocket}

A small rocket is pointed straight up and fired. Its motor burns for $\tau=10$ s; while the rocket's motor burns, it
accelerates upward at $a_r=15 \rm m/\rm s^2$; after it burns out, the rocket is in freefall. (To make the numbers simpler for 
this problem, and for every problem in this class, you may use $g=10\, \rm m/\rm s^2$.)

\begin{enumerate}
\item{Before you do any mathematics, sketch graphs on the rest of this page for the rocket's acceleration vs. time, velocity vs. time, and position vs. time, in that order. (Here I've labeled the height $y$ rather than $x$ since it is moving vertically.) Remember:

\BI
\item The slope of the velocity graph should be equal to the value of the acceleration graph, since acceleration is the derivative of velocity
\item The slope of the position graph should be equal to the value of the velocity graph.
\EI

This means that:

\BI
\item When the acceleration is positive, the velocity should be increasing, and the position should be concave up;
\item When the acceleration is negative, the velocity should be decreasing, and the position should be concave down;
\item When the velocity is zero, the position graph should have a maximum or minimum
\EI

{\color{red}The students {\it should not be expected to have the numbers correct} on these graphs. This is not the point; they don't know the numbers yet. What they should understand is how the shapes of the graphs look before and after burnout. See the graphs I've made on the next page.
	
	
	\begin{itemize}
		\item The acceleration graph is a positive constant before burnout, and then changes (discontinuously) to a negative constant after burnout
		\item The velocity graph is a straight line with a positive slope before burnout, and then changes (without a discontinuity, but with a cusp) to a negative slope after burnout
		\item The position graph is a concave up parabola while the motor is burning, and a concave down parabola after burnout; the point of burnout is an {\it inflection point}, with no sudden change in the slope at that point
		\item The rocket continues to go upward even after the motor burns out
		\item The highest point is when the velocity reaches zero again, 15 seconds {\it after} burnout
			\end{itemize}
			
			The graphs here have numbers just because that's how I made them. The students will not have numbers. That's okay. 
}
\newpage


\begin{center}
\includegraphics[width=0.55\textwidth]{acceleration.pdf}\\
\includegraphics[width=0.55\textwidth]{velocity.pdf}\\
\includegraphics[width=0.55\textwidth]{position.pdf}\\
\end{center}
\newpage
Since the rocket's acceleration changes in flight, you can't use the constant-acceleration kinematics
formulae we've learned to understand the whole flight at once. However, the acceleration {\it is} piecewise constant.

This means that {\it one} copy of the constant-acceleration kinematics relations won't get the job done, but {\it two} 
copies will.

In problems like these, take the following approach:

\BI
\item Identify the moment in time when the acceleration changes (in this case, when the motor runs out of fuel)
\item Write down one set of kinematics relations (velocity and position) for the phase of the motion while the motor is on. Here, your time variable should represent the time since the rocket was launched, and your initial position and velocity are the position and velocity it had when it was launched (in this case, zero). 
\item Write down a second set of kinematics relations for the phase of the motion after the acceleration changes and the rocket is only moving under the influence of gravity. Here, your time variable represents the time since the motor turned off, since that is when this set of kinematics relations became valid.
\item Use the first set of kinematics relations to solve for the position and velocity at burnout. (Call this $y_b$ and $v_b$, or whatever other variables you want.) These values will become the initial position and velocity for the second phase. 
\item Solve for whatever you want to know about the second phase.
\EI

{\color{red}This idea is a bit hard for some of them since there's a lot to keep track of. The ones who memorize formulas will get tripped up in ``initial'' and ``final'' whatnots, and confuse themselves with notation. I prefer to label the intermediate value as $y_b$, etc. -- ``the position at burnout''. That way it's clear what it is -- it's the ``final'' position for the first phase and the ``initial'' one for the second phase.}

Answer the following. Ideally, figure out your answers in terms of $\tau$ (the rocket burn time), $g$, and $a_r$. You don't 
even have to plug numbers in if you don't want to!

\item How high above the ground is the rocket once its motor burns out? (Call this $y_b$.)
{\color{red} 

$$y_b = \frac{1}{2}a_r\tau^2 = \frac{1}{2} (15 \rm m/\rm s^2) (10 s)^2 = 750 \rm m$$

}
\newpage
\item How fast is the rocket traveling once its motor burns out? (Call this $v_b$.)

{\color{red}$$v_b = a_r\tau =  (15 \rm m/\rm s^2) (10 s) = 1505 \rm m/\rm s$$}


\item How fast is the rocket traveling when it reaches its maximum height?

{\color{red} This one's a ``think, not calculate'' -- at the maximum height the velocity is zero.}


\item What is that maximum height?

{\color{red} Now we have to do a bit more work. The position and velocity equations for the second phase of the motion after burnout, where $t'$ is the time since burnout (Note for students: $t'$ means ``the other variable like $t$ but not the same'', i.e. ``a different time''. It doesn't mean derivative.):
	
	\begin{align*}
		x(t') &= x_b + v_b t' - \frac{1}{2}gt'^2\\
		v(t') &= v_b - gt'
	\end{align*}
	
	We know $v_b = a_r\tau$. 	Set the second of these equal to zero and get:
	
	$$t' = \tau \frac{a_r}{g}$$
	
	This gives us the time from burnout to maximum height. Then substitute into $x(t')$:
	
	\begin{align*}
		x(t') &= x_b + v_b t' - \frac{1}{2}gt'^2 \\
		x(t') &= \frac{1}{2}a_r\tau^2 + a_r\tau t' - \frac{1}{2}g\tau^2 \left(\frac{a_r}{g}\right)^2 \\
		&=\frac{1}{2}g\tau^2 + a\tau t' - \frac{\tau^2a_r^2}{2g}
	\end{align*}
	
	Putting in the numbers for that last bit we get a maximum height of 1875 m.
	
		
}

\item How long does it take for the rocket to land back on the ground?

{\color{red} Here there are two reasonable approaches:
	
	\begin{itemize}
		\item ``Start thinking from the maximum height'': if they found the maximum height as 1875 meters in the last step, just ask ``how long does it take something to fall from 1875 m. Then add the time you get from this to the time since burnout, and add {\it that} to the time from launch to burnout, to get the total time.
		
		\item ``Start thinking from burnout'': the motion after that point is a parabola, so write down that equation of motion and set the position to zero. This is
		
		$$	0 = x_b + v_b t' - \frac{1}{2}gt'^2$$
		
		which is a quadratic equation. Put in the known values of $x_b$ and $v_b$ and solve for $t'$.
	\end{itemize}
	


}
}

\end{enumerate}
\end{document}

