\documentclass[12pt]{article}
\setlength\parindent{0pt}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{xcolor}
\setlength{\parskip}{4mm}
\usepackage[left=2cm, right=2cm, top=1.5cm, bottom=1cm]{geometry}
%\usepackage[margin=0.5in, paperwidth=13.5in, paperheight=8.4375in]{geometry}
\def\LL{\left\langle}   % left angle bracket
\def\RR{\right\rangle}  % right angle bracket
\def\LP{\left(}         % left parenthesis
\def\RP{\right)}        % right parenthesis
\def\LB{\left\{}        % left curly bracket
\def\RB{\right\}}       % right curly bracket
\def\PAR#1#2{ {{\partial #1}\over{\partial #2}} }
\def\PARTWO#1#2{ {{\partial^2 #1}\over{\partial #2}^2} }
\def\PARTWOMIX#1#2#3{ {{\partial^2 #1}\over{\partial #2 \partial #3}} }
\newcommand{\BI}{\begin{itemize}}
\newcommand{\EI}{\end{itemize}}
\newcommand{\BE}{\begin{displaymath}}
\newcommand{\EE}{\end{displaymath}}
\newcommand{\BNE}{\begin{equation}}
\newcommand{\ENE}{\end{equation}}
\newcommand{\BEA}{\begin{eqnarray}}
\newcommand{\EEA}{\nonumber\end{eqnarray}}
\newcommand{\EL}{\nonumber\\}
\newcommand{\la}[1]{\label{#1}}
\newcommand{\ie}{{\em i.e.\ }}
\newcommand{\eg}{{\em e.\,g.\ }}
\newcommand{\cf}{cf.\ }
\newcommand{\etc}{etc.\ }
\newcommand{\Tr}{{\rm tr}}
\newcommand{\etal}{{\it et al.}}
\newcommand{\OL}[1]{\overline{#1}\ } % overline
\newcommand{\OLL}[1]{\overline{\overline{#1}}\ } % double overline
\newcommand{\OON}{\frac{1}{N}} % "one over N"
\newcommand{\OOX}[1]{\frac{1}{#1}} % "one over X"

\def\BS{\bigskip}

\begin{document}
\pagenumbering{gobble}
\Large
\centerline{\sc{Recitation Questions}}
\normalsize

\centerline{\sc{Week 3, Day 1 Instructor Guide - 2D Motion}}

\it These recitation problems, along with those on your homework, will prepare you very well for the group exam in your second recitation this week and the exam next Tuesday. In these problems you will practice:


\rm

\BI
\item Working with vector quantities as separate $x-$ and $y-$components
\item Writing down the equations of motion that describe an object's motion in two dimensions
\item Interpreting statements in words about motion in two dimensions as statements about algebraic variables
\item Solving those equations algebraically as directed by your statements
\EI




\newpage

\rm 



\centerline{\Large Question 1: a hiker\footnote{\noindent This problem is based on a true story; the hiker was a long-time PHY211 coach who graduated from ESF in 2018. Yes, she really threw a boot into a stream. (She's also the same one with the one-eyed kitten.)} crosses a stream}     

A hiker in the Adirondacks encounters a stream that is too wide to jump across. So she doesn't get her boots wet, she takes them off and throws them across before walking barefoot through the water. 

Suppose that the stream is $d=12$ m across, and she throws her boot from
ground level at an angle $\theta = 35^\circ$ above the horizontal.

First, you will calculate the minimum velocity she must throw the boot with to get it across the stream. Then you'll figure out what happens if she throws it at this speed but at a different angle than she intended to.

{\color{red}
	
	We're asking them to solve for velocity here for a sneaky reason: it means they have to do a substitution with variables from the velocity to the position equation (or vice versa) {\it before} plugging in numbers.
	
	This question walks them through the following steps, which will apply to all similar sorts of questions:
	
	\begin{enumerate}
		\item Draw a picture. Choose a coordinate system: this consists of picking a location to be your origin (x=0, y=0) and picking which directions are positive.
		\item Write down position and velocity as a function of time in $x$ and $y$
		\item Substitute in things that you know; in this case you know the angle so you should write $v_{0,x}$ as $v_0 \cos \theta$, etc. You also know $a_x$, $a_y$.
		\item Write down a sentence that asks your question in terms of your algebraic variables
		\item Do the algebra (probably a substitution) that your sentence tells you to do; solve for the variable you want. 
		\item Only then put in the numbers.
	\end{enumerate}
	
}

\begin{enumerate}

\item Draw a diagram of the boot's path in the air. Choose a coordinate system: what point are you considering ($x=0, y=0$), and which directions are positive? {\it (This is important because it gives you a picture that orients you to how the boot moves, and the coordinate system lets you translate the picture into mathematics.)}

{\color{red}Here pretty much everyone will pick the coordinate system where she's at the origin and `up' and `across' are positive.
}

\item You know the initial velocity vector $\vec v_0$ as a magnitude and direction, but to do your calculations you will need to know its $x-$ and $y-$components. By doing trigonometry,
determine $v_{x,0}$ and $v_{y,0}$ in terms of $v_0$, $\sin \theta$, and $\cos \theta$. {\it (Since it is easiest to work with $x-$ and $y-$ components, you will want to convert any vectors given to you in magnitude/direction form to components first.)}

{\color{red}
They will likely have to draw the triangle, but they should get:
\begin{align*}
	v_{0,x} = v_0 \cos \theta \\
	v_{0,y} = v_0 \sin \theta 
\end{align*}

Note that they should {\it not} memorize ``the x-component is cosine, the y-component is sine''. We won't always give the angle with the horizontal. They have to think about it (and probably draw the triangle) each time.
}


\item Write expressions for the $x-$ and $y-$components of its position and velocity as a function of time. These expressions will have lots of variables in them ($a_x$, $a_y$, $v_{x,0}$, $v_{y,0}$, $x_0$, and $y_0$) -- that's okay. {\it (It is always a good idea to work from ``general'' to ``specific''; writing down the equations of motion in the most general way and then substituting in what you know will make sure you don't go astray.)}

{\color{red}
	
	In the most general form we have:
	
	\begin{minipage}{0.5\textwidth}
		\begin{align*}
			x(t) = \frac{1}{2}a_xt^2 + v_{0,x} t + x_0 \\
			y(t) = \frac{1}{2}a_yt^2 + v_{0,y} t + y_0 
		\end{align*}
	\end{minipage}
	\begin{minipage}{0.5\textwidth}
		\begin{align*}
			v_x(t) = a_xt + v_{0,x} \\
			v_y(t) = a_yt + v_{0,y} 
		\end{align*}
	\end{minipage}
}

\item Do you know anything about any of those variables? If so, which ones? {\it (It is always a good idea to keep track of things you know and things you want to find. Here, you know something about the starting velocity and the acceleration.)}

{\color{red}
We know the things we figured out about the initial velocity in part (2), and also know the acceleration and initial position. Then we have:

	\begin{minipage}{0.5\textwidth}
	\begin{align*}
		x(t) &= (v_0 \cos \theta) t \\
		y(t) &= -\frac{1}{2}gt^2 + (v_0 \sin \theta) t 
	\end{align*}
\end{minipage}
\begin{minipage}{0.5\textwidth}
	\begin{align*}
		v_x(t) &= v_0 \cos \theta \\
		v_y(t) &= -gt + v_0 \sin \theta
	\end{align*}
\end{minipage}	
	
	
}


\item With what initial velocity $v_0$ must she throw her boot in order to get it across the stream? {\it (I've skipped some steps here for you: you will want to write down a sentence in terms of your algebraic variables that answers the question, then do the algebra.)}

{\color{red}
	The sentence they want is ``What must the value of $v_0$ be such that the boot's x-coordinate $x(t)$ is equal to $d$ at the same time that the boot's y-coordinate $y(t)$ is equal to 0?''
	
	In this case ``x-coordinate equals $d$'' means ``reaches the other side of the stream'' and ``y-coordinate equals 0'' means ``hits the ground''. So this says ``the boot hits the ground only after it crosses the stream''.
	
	Doing the math, then:
	
	\begin{align*}
			d =& (v_0 \cos \theta) t \\
			0 =& -\frac{1}{2}gt^2 + (v_0 \sin \theta) t 
	\end{align*}
	
	Solve the first equation for $t$ to get $t = \frac{d}{v_0 \cos \theta}$. Then substitute into the other, canceling a factor of $t$ (which we can do since we already know we don't care about the $t=0$ solution) to get 
	
	$$0 = -\frac{gd}{2v_0 \cos \theta} + v_0 \sin \theta$$
	
	which tells us that 
	
	$$v_0 = \sqrt { \frac{gd}{2 \sin \theta \cos \theta}}$$
	
}

\item Suppose that she accidentally throws her second boot with the same initial velocity $v_0$ but at an angle $\theta = 65^\circ$ above the horizontal. Where will it land?

{\color{red}
	
	The great thing about doing these things with variables is that I can just solve the relationship above for a different variable to find where it lands ($d$). Or I could start from the beginning, asking this time ``What is the value of $x(t)$ at the time that $y(t)$ equals zero?'' 
	
	This gives:
	
	$$ 0 = -\frac{1}{2}gt^2 + (v_0 \sin \theta) t \rightarrow t = \frac{2v_0 \sin \theta}{g}$$
	
	Substituting to find $x(t)$ gives 
	
	$$x(t) =  \frac{2v_0^2 \cos \theta \sin \theta}{g}$$
}

\end{enumerate}

\newpage

\centerline{\Large Question 2: a prankster}     

\footnotesize

\it \begin{center} The students in the next two problems are based on two more of our past PHY211 coaches. \\He is now an officer in the US Air Force; she works in environmental reclamation and remediation.\end{center}

\normalsize \bigskip\rm

A mischievous SUOC student has climbed on the roof of a snow-covered building and is trying to hit her friend with snowballs as he walks through the Quad. 
She throws them at an angle of $\theta$ above the horizontal at a speed of $v_0$. 
The building has a height $h$. {\it (In this problem, you will think about how to solve for various things, but not actually do the algebra. As with any problem where some variables are specified in the statement, you can use
	those variables in your answer -- if you were doing the math on this problem, you would have $v_0$'s, $\theta$'s, $h$'s, and $g$'s in your answer.)}

\begin{enumerate}

\item Draw a cartoon of the problem, making clear your coordinate system and origin, and
labelling interesting things.

{\color{red} In this case there are multiple sensible choices for coordinate system. All are valid and will give the same answer. But here let's choose the base of the building as the origin and the direction pointing toward the student on the Quad as the positive x-direction.}


\item Write expressions for $x(t)$, $y(t)$, $v_x(t)$, and $v_y(t)$, substituting in variables that you know. {\it (Some will be zero; $v_0$, $\sin \theta$, $\cos \theta$, $h$, and $g$ will make an appearance.)}

{\color{red}
	With those coordinates:
	
	\begin{minipage}{0.5\textwidth}
		\begin{align*}
			x(t) &= (v_0 \cos \theta) t \\
			y(t) &= -\frac{1}{2}gt^2 + (v_0 \sin \theta) t + h
		\end{align*}
	\end{minipage}
	\begin{minipage}{0.5\textwidth}
		\begin{align*}
			v_x(t) = v_0 \cos \theta \\
			v_y(t) = -gt + v_0 \sin \theta
		\end{align*}
	\end{minipage}	
	
}


\newpage
\item Write sentences in terms of your algebraic variables that allow you to answer the following. You  
will need to incorporate vector language at times: for instance, you may need to use terms like ``the magnitude of the
velocity vector'' (which will require you to solve for both $v_x$ and $v_y$.)

{\color{red} They are not going to want to do this since they think this is a math class half the time. But in practice -- this is the stuff they mess up on the exams even more than the math. If they are able to do this, that's most of the challenging aspects.}
\BI
\item How much time does it take for the snowballs to hit the Quad?
{\color{red} ``What is the value of $t$ when $y(t) = 0$?''}


\item Where do the snowballs land on the Quad?

{\color{red} ``What is the value of $x(t)$ at the time that $y(t) = 0$?''}

\item How fast are the snowballs traveling when they hit the Quad?

{\color{red} ``What is the magnitude of the velocity vector $\vec v(t)$ at the time that $y(t) = 0$?'' (This will be $\sqrt{v_x^2 + v_y^2}$, of course.)}

\item In what direction are they moving when they land on the Quad?

{\color{red} ``In what direction is the vector $\vec v(t)$ pointing at the time that $y(t) = 0$?'' (This will involve $\tan^{-1} \frac{v_x}{v_y}.$)}
\EI
\end{enumerate}

\newpage

\centerline{\Large Question 3: retaliation!}     

He decides to throw a snowball back at her. He's standing a distance $d$ from the side of the building, and 
throws a snowball at an angle $\theta$ above the horizontal at a speed $v_0$. However, the snowball slips out of his hand when he throws it,
and it doesn't go very fast -- instead of hitting her on top of the building, it hits the side of the building.

\begin{enumerate}

\item Draw a cartoon of the problem, making clear your coordinate system and origin, and
labelling interesting things.

{\color{red} They can choose the same coordinate system as before, or a different one. I'll choose the same one -- this means that the initial x-position is $d$ and the initial $v_x$ is negative.}


\item Write expressions for $x(t)$, $y(t)$, $v_x(t)$, and $v_y(t)$, substituting in variables that you know.

{\color{red}
	With those coordinates:
	
	\begin{minipage}{0.5\textwidth}
		\begin{align*}
			x(t) &= (-v_0 \cos \theta) t + d\\
			y(t) &= -\frac{1}{2}gt^2 + (v_0 \sin \theta) t 
		\end{align*}
	\end{minipage}
	\begin{minipage}{0.5\textwidth}
		\begin{align*}
			v_x(t) = -v_0 \cos \theta \\
			v_y(t) = -gt + v_0 \sin \theta
		\end{align*}
	\end{minipage}	
	
}


\item Write a sentence in terms of your algebraic variables that will let you figure out how far above the ground 
the snowball hits the side of the building.


{\color{red} ``What is the value of $y(t)$ at the time that $x(t)=0$?'' -- in my coordinates. Many people will have the building as $x(t) = d$ or $-d$. That's fine!}


\newpage

\item Based on your sentence, figure out how far above the ground the snowball hits the building. Your answer should be 
in terms of $v_0$, $\theta$, $d$, and $g$.

{\color{red}
	
	Now we just do that algebra. Set $x(t)=0$ to find $t$:
	
	$$			0 = (-v_0 \cos \theta) t + d \rightarrow t = \frac{d}{v_0 \cos \theta}$$
	
	Then put that into $y(t)$ to find the height.
}

%\vspace{3in}
%
%\item He doesn't give up, though, and throws another snowball at her -- again at an angle $\theta$ above the horizontal.
%He throws this one harder, and it hits her feet as she stands on the edge of the building. Write a sentence in terms of your algebraic variables that will let you figure out how fast he had to throw it. 
%
%\vspace{1in}
%
%\item Now, based on your previous sentence, figure out the initial speed of the second snowball he threw. {\it (Your answer will have $h$, $g$, $v_0$, $d$, and $\theta$ in it, since those variables are given to you in the problem.)}

\end{enumerate}
%\newpage
%\newpage
%
%\begin{flushright}Your name and email: \underline{\hspace{2in}} \hspace{2em} Your TA's name:\underline{\hspace{1.5in}}
%	
%	
%	
%\end{flushright}
%\begin{center}
%	\large Retrospective for Week 3 Recitation 1
%\end{center}
%\small 
%What are the names of the other people in your group?
%
%\vspace{0.7in}
%
%
%Question 1 (the `hiker and boot problem'') is designed to give you experience with two-dimensional motion with constant acceleration.
%
%After today's recitation, what is your assessment of your ability to do this sort of thing? Check one or more:
%
%\begin{itemize}
%	\setlength{\itemsep}{0pt}
%	\setlength{\parskip}{0pt}
%	\renewcommand\labelitemi{{\large[ ]}}
%	\item 1. I already knew how to do this before today's recitation
%	\item 2. During this recitation I figured out how to do this, and understand this problem now after my practice today
%	\item 3. Our group worked on this exercise, but I don't understand all of the details involved
%	\item 4. I don't understand this concept very well at all
%
%	\vspace{1em}
%	\item I would like to attend a study session focusing on the conceptual aspects of this exercise
%\end{itemize}
%
%Question 2 is designed to give you practice translating physical questions into mathematical recipes. Tell us how you found this exercise:
%
%\begin{itemize}
%	\renewcommand\labelitemi{{\large[ ]}}
%	\setlength{\itemsep}{0pt}
%	\setlength{\parskip}{0pt}
%	\item 1. I already knew how to do this before today's recitation
%	\item 2. During this recitation I figured out how to do this, and understand this problem now after my practice today
%	\item 3. Our group worked on this exercise, but I don't understand all of the details involved
%	\item 4. I don't understand this concept very well at all
%	\item 5. I didn't get to work on this exercise
%		\vspace{1em}
%	\item I would like to attend a study session focusing on things like this exercise
%	\vspace{1em}
%
%\end{itemize}
%
%Please leave any other feedback that you have for us below or on the back.
%


\end{document}
