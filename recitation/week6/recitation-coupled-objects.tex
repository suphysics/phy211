\documentclass[12pt]{article}
\setlength\parindent{0pt}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[margin=1.5cm]{geometry}
\usepackage{color}
\definecolor{Red}           {rgb}{1,0.3,0.0}
\setlength{\parskip}{4mm}
\def\LL{\left\langle}   % left angle bracket
\def\RR{\right\rangle}  % right angle bracket
\def\LP{\left(}         % left parenthesis
\def\RP{\right)}        % right parenthesis
\def\LB{\left\{}        % left curly bracket
\def\RB{\right\}}       % right curly bracket
\def\PAR#1#2{ {{\partial #1}\over{\partial #2}} }
\def\PARTWO#1#2{ {{\partial^2 #1}\over{\partial #2}^2} }
\def\PARTWOMIX#1#2#3{ {{\partial^2 #1}\over{\partial #2 \partial #3}} }
\newcommand{\BI}{\begin{itemize}}
\newcommand{\EI}{\end{itemize}}
\newcommand{\BE}{\begin{displaymath}}
\newcommand{\EE}{\end{displaymath}}
\newcommand{\BNE}{\begin{equation}}
\newcommand{\ENE}{\end{equation}}
\newcommand{\BEA}{\begin{eqnarray}}
\newcommand{\EEA}{\nonumber\end{eqnarray}}
\newcommand{\EL}{\nonumber\\}
\newcommand{\la}[1]{\label{#1}}
\newcommand{\ie}{{\em i.e.\ }}
\newcommand{\eg}{{\em e.\,g.\ }}
\newcommand{\cf}{cf.\ }
\newcommand{\etc}{etc.\ }
\newcommand{\Tr}{{\rm tr}}
\newcommand{\etal}{{\it et al.}}
\newcommand{\OL}[1]{\overline{#1}\ } % overline
\newcommand{\OLL}[1]{\overline{\overline{#1}}\ } % double overline
\newcommand{\OON}{\frac{1}{N}} % "one over N"
\newcommand{\OOX}[1]{\frac{1}{#1}} % "one over X"



\begin{document}
\pagenumbering{gobble}
\Large
\centerline{\sc{Recitation Exercises}}
\normalsize
\centerline{\sc{Week 6, Day 1}}

\medskip
\centerline{\Large Question 1: Ball and Rotating Pole}

\begin{minipage}{0.5\textwidth}
	Consider a ball tethered to a rotating pole by two cables of equal length as shown to the right. The ball rotates along with the pole, making a horizontal circle (shown in green on the diagram). Suppose that you know the ball has a mass $m$, the cables make an angle $\theta$ with the horizontal, the pole is rotating at angular velocity $\omega$, and the radius of the circle it makes is $r$.
	
	\bigskip
	
	You want to find the relationship between $m$, $\omega$, $r$, and the tensions in the cables $T_1$ and $T_2$.
	
\end{minipage}
\begin{minipage}{0.4\textwidth}
	\hspace{0.1\textwidth}
	\includegraphics[width=0.9\textwidth]{pole-crop.pdf}
\end{minipage}

\begin{enumerate}
	\item Draw a force diagram for the ball below, and indicate your choice of coordinate system.

\vspace{3in}

\item Construct Newton's laws in both $x$ and $y$ for the ball based on your force diagram, putting in what you know about $a_x$ and $a_y$. 

\newpage

\item Determine $T_1$ and $T_2$ in terms of $m$, $g$, $r$, and $\theta$.


\vspace{4in}
\item Show that in the limit where the pole is spinning extremely quickly (where $\omega$ becomes large) the two tension forces become equal. Is this what you expect? Explain why.

\end{enumerate}

%
%{\color{Red}
%
%\begin{align*}
%T_1 \cos \theta + T_2 \cos \theta\hspace{2.6em} &= m \omega^2 r \\
%T_1 \sin \theta - T_2 \sin \theta - mg &= 0
%\end{align*}
%
%They shouldn't feel the need to go any further than this.


\newpage

\centerline{\Large Question 2: horse and sleigh}

{\it (This is a former exam problem from the COVID-era class in 2021. In that problem the horse was named Latte and belonged to one of our students named Alicia.)}
\medskip

Suppose a horse of mass $m_h$ is trying to pull a sleigh up a snow-covered hill that slopes gently upward at an angle $\theta$. The coefficient of static friction between the horse's hooves and the snow is $\mu_s$; the coefficient of friction between the sleigh's bottom and the snow is $\mu_k$.

You would like to determine the most massive sleigh that he can pull up the slope. Suppose that he is just trying to pull the sleigh at a constant velocity, keeping it moving up the slope.

\begin{enumerate}
	\item Draw force diagrams for Latte and the sleigh he is pulling. Think carefully about the direction that friction points for each object.
	
	\vspace{3in}
	
\item Write down an expression of Newton's second law $\sum F = m a$ for each object in each direction. (You should have four equations total.)

\vspace{3in}
\newpage

\item Solve this system of equations to find out the most massive sleigh that Latte can pull; give your answer in terms of $m_h$, $\theta$, $\mu_s$, and $\mu_k$.

\vspace{5in}

\item Determine the tension in the cable when Latte is pulling this sled in terms of $m_h$, $\theta$, $\mu_s$, and $\mu_k$. 

\newpage


\end{enumerate}

\centerline{\Large Question 3: Friction and Newton's Third Law}

A stack of two books, each of mass 1 kg, sits on a table. The coefficients of static friction between the books, and between the bottom book and the table, are 0.4; the coefficients of kinetic friction are 0.2. A person exerts a sudden horizontal force on the bottom book. Intuitively, we know what happens:

\begin{itemize}
	\item{I. If this force is moderate, then the two books accelerate together at the same rate.}
	\item{II. If this force is very large, then the bottom book is yanked out from beneath the top one, and they accelerate at different rates.}
\end{itemize}

a) In case I, what type of friction exists between the bottom book and the table? What about between the two books?

\vspace{0.5in}

b) In case II, what type of friction exists between the bottom book and the table? What about between the two books?

\vspace{0.5in}

c) Draw force diagrams for case II -- the situation when the force is large enough to pull the bottom book out from underneath the top one. {\it This is difficult} -- think carefully about what forces are present. Make sure you use clear symbols: don't write ``$F_{\rm fric}$'', since there are multiple frictional forces present.

\vspace{2.3in}

d) Which Newton's-third-law pairs are present in your force diagram? Verify that your diagrams satisfy Newton's third law; call a TA or coach over and show them your force diagrams.

\newpage

e) Suppose a person applies a horizontal force of 10 N to the bottom book. {\it (This is large enough to pull the bottom book out from under the top book.)} Determine the acceleration of both books.

\end{document}
