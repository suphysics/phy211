\documentclass[12pt]{article}
\setlength\parindent{0pt}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{xcolor}
\usepackage[margin=1.5cm]{geometry}
\usepackage{graphicx}
\setlength{\parskip}{4mm}
\def\LL{\left\langle}   % left angle bracket
\def\RR{\right\rangle}  % right angle bracket
\def\LP{\left(}         % left parenthesis
\def\RP{\right)}        % right parenthesis
\def\LB{\left\{}        % left curly bracket
\def\RB{\right\}}       % right curly bracket
\def\PAR#1#2{ {{\partial #1}\over{\partial #2}} }
\def\PARTWO#1#2{ {{\partial^2 #1}\over{\partial #2}^2} }
\def\PARTWOMIX#1#2#3{ {{\partial^2 #1}\over{\partial #2 \partial #3}} }
\newcommand{\BE}{\begin{displaymath}}
\newcommand{\EE}{\end{displaymath}}
\newcommand{\BNE}{\begin{equation}}
\newcommand{\ENE}{\end{equation}}
\newcommand{\BEA}{\begin{eqnarray}}
\newcommand{\EEA}{\nonumber\end{eqnarray}}
\newcommand{\EL}{\nonumber\\}
\newcommand{\la}[1]{\label{#1}}
\newcommand{\ie}{{\em i.e.\ }}
\newcommand{\eg}{{\em e.\,g.\ }}
\newcommand{\cf}{cf.\ }
\newcommand{\etc}{etc.\ }
\newcommand{\Tr}{{\rm tr}}
\newcommand{\etal}{{\it et al.}}
\newcommand{\OL}[1]{\overline{#1}\ } % overline
\newcommand{\OLL}[1]{\overline{\overline{#1}}\ } % double overline
\newcommand{\OON}{\frac{1}{N}} % "one over N"
\newcommand{\OOX}[1]{\frac{1}{#1}} % "one over X"



\begin{document}
\pagenumbering{gobble}
\Large
\centerline{\sc{Recitation Exercises}}
\normalsize
\centerline{\sc{Week 5 Day 2}}



A person uses their arm to spin a bucket in a vertical circle at a constant speed; the radius of the circle is 80 cm. The bucket goes around the circle once every second. Inside the bucket is a friendly frog of mass 500 grams. 


a) Draw a force diagram for the frog when the bucket is at the top of the circle, and when it is at the bottom. Show your force diagram to your TA or coach. {\it (Don't overcomplicate this -- make sure your arrows only represent real forces!)}

{\color{red} At the bottom, gravity points down and the normal force up. At the top, both gravity and the normal force point down (since the bucket is upside down). {\bf There are no other forces.} Students will want to add all kinds of things that are not forces to the force diagrams. The biggest challenge here is not making stuff up.}


b) What is the acceleration of the bucket? {\it (Think about both its magnitude and direction.)}

{\color{red} The acceleration is $mw^2 r$ toward the center -- note that $\omega = 2\pi$ radians/sec. This is upward at the bottom and downward at the top.}

c) As you saw this week in your homework, your ``apparent weight'' is simply the magnitude of the normal force that an object under your feet exerts on you. What is the frog's apparent weight at the bottom and at the top of the circle?

{\color{red}
	
	Here, again, the challenge will be to convince students to trust that Newton's laws of motion work and that they don't need to make up stuff. 
	
	Top of circle, using a coordinate system where downward is positive:
	
	\begin{align*}
		\sum F &= ma \\
	mg + F_N &= m\omega^2 r \\
	F_N &= m\omega^2 r - mg
	\end{align*}
	
	Bottom of circle, using a coordinate system where upward is positive:
	
		\begin{align*}
		\sum F &= ma \\
	   F_N - mg &= m\omega^2 r \\
		F_N &= m\omega^2 r + mg
	\end{align*}
	
	Astute students will realize here that the apparent weight could be {\it negative} at the top if $\omega$ is small, but will be always greater than the weight of the frog at the bottom.
	
}


\vspace{2in}
\newpage
d) Explain why the frog doesn't fall out of the bucket at the top of the swing, despite the fact that the only forces acting on it point downward. This is a pretty subtle but important point -- you should talk about it for a while and call a coach or TA over to join your conversation.

{\color{red} There are a bunch of different arguments here. They all center on the realization that the frog {\it is} falling, just not falling {\it out of the bucket}, since the bucket is accelerating downward. Different pieces of logic will click for different students, but here are some of them:
	
	\begin{itemize}
		\item The bucket is accelerating downward at $\omega^2 r$. If this is bigger than $g$, then the bucket is not only falling, it's falling {\it faster} than the frog would fall on its own, so the bottom of the bucket must push on the frog to make it fall {\it faster}.
		\item The frog would continue horizontally in a straight line if the bucket disappeared at the top, curving downward slowly under the influence of gravity. But the bucket is really curving downward faster than that (since $\omega^2 r > g$), so the bucket ``catches'' the frog as it falls.
		\item Imagine an elevator accelerating downward. If it is in freefall, things in it will appear to float inside. But if it is accelerating downward faster than $g$, things in it will be stuck to the ceiling, since the ceiling must push on them to make them accelerate downward so fast.
	\end{itemize}
	
}
e) Now, imagine that the person swinging the bucket slows down gradually. At some point, the frog will fall out of the bucket. (It's a frog, so it'll land on its feet and not be hurt!) How low can $\omega$ become before the frog falls out of the bucket? {\it (Hint: The frog is just sitting in the bucket. We haven't glued it to the bottom -- we are kind to animals in our class!)}

{\color{red}
The critical insight here is that the normal force must be directed downward (so $F_N$ must be positive). If it becomes negative, then this means that the bucket must apply an {\it upward} force to the frog to keep it in the bucket. Since this cannot happen, the frog falls.

Another piece of logic: all of the preceding arguments for why ``the frog doesn't fall out of the bucket'' require that $\omega^2 r > g$. If $\omega^2 r$ is equal to or less than $g$, then they are all invalid.

}
\newpage


A highway curve has a radius of curvature of 500 meters; that is, it is a segment of a circle whose radius is 500 m. It is banked so that traffic moving at 30 m/s can travel
around the curve without needing any help from friction.


a) Draw a force diagram for a car traveling around this curve at a constant speed. Draw the diagram so that you are looking at the rear of the car. {\it Hint:} Do not tilt your coordinate axes for this problem!


{\color{red}This will be quite challenging for many of them, again because they will want to make stuff up.  We did a similar example (car on flat curve) in 9:30 section, but not in 11:00.


The force diagram has:

\begin{itemize}
	\item A normal force, pointing diagonally upward and toward the inside of the curve
	\item Gravity
	\item {\bf nothing else}
\end{itemize}
}


b) What is the acceleration of the car in the $x-$direction? What about the $y-$direction?

{\color{red} The reason we don't tilt the axes here is so the acceleration lines up with the coordinates. Note that in this case the {\it normal force}, not gravity, is the force vector that needs to be broken into components.  The acceleration is $v^2/r$ inward.}
\vspace{1in}

c) Write down two copies of Newton's second law in the $x-$ and $y-$directions, as you have always done here.
{\color{red}
	
This gives:

\begin{align*}
	\sum F_x =& ma_x: & F_N \sin \theta =& m\frac{v^2}{r} \\
	\sum F_y =& ma_y: & F_N \cos \theta - mg =& 0
\end{align*}
}


\newpage

d) Solve the resulting system of two equations to determine the banking angle of the curve.
{\color{red}
	
Maffs:

\begin{align*}
	F_N =& \frac{mg}{\cos \theta} \\
	mg \frac{\sin \theta}{\cos \theta} =& m\frac{v^2}{r}\\
	\theta =& \tan^{-1} \sqrt{\frac{v^2}{gr}}
\end{align*}
}
\end{document}
