
\documentclass[12pt]{article}
\setlength\parindent{0pt}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage[margin=0.5in]{geometry}
\usepackage{xcolor}
\usepackage{graphicx}
\setlength{\parskip}{4mm}
\def\LL{\left\langle}   % left angle bracket
\def\RR{\right\rangle}  % right angle bracket
\def\LP{\left(}         % left parenthesis
\def\RP{\right)}        % right parenthesis
\def\LB{\left\{}        % left curly bracket
\def\RB{\right\}}       % right curly bracket
\def\PAR#1#2{ {{\partial #1}\over{\partial #2}} }
\def\PARTWO#1#2{ {{\partial^2 #1}\over{\partial #2}^2} }
\def\PARTWOMIX#1#2#3{ {{\partial^2 #1}\over{\partial #2 \partial #3}} }
\newcommand{\BE}{\begin{displaymath}}
	\newcommand{\EE}{\end{displaymath}}
\newcommand{\BNE}{\begin{equation}}
	\newcommand{\ENE}{\end{equation}}
\newcommand{\BEA}{\begin{eqnarray}}
	\newcommand{\EEA}{\nonumber\end{eqnarray}}
\newcommand{\EL}{\nonumber\\}
\newcommand{\la}[1]{\label{#1}}
\newcommand{\ie}{{\em i.e.\ }}
\newcommand{\eg}{{\em e.\,g.\ }}
\newcommand{\cf}{cf.\ }
\newcommand{\etc}{etc.\ }
\newcommand{\Tr}{{\rm tr}}
\newcommand{\etal}{{\it et al.}}
\newcommand{\OL}[1]{\overline{#1}\ } % overline
\newcommand{\OLL}[1]{\overline{\overline{#1}}\ } % double overline
\newcommand{\OON}{\frac{1}{N}} % "one over N"
\newcommand{\OOX}[1]{\frac{1}{#1}} % "one over X"

\pagenumbering{gobble}

\begin{document}
	\Large
	\centerline{\sc{Recitation Exercises}}
	\normalsize
	\centerline{\sc{Week 9 Day 1}}
	
	
Most submarines throughout history have propelled themselves using electric batteries underwater. This limits their endurance when operating underwater since the same mass of batteries does not store as much energy as diesel. When surfaced, they use diesel generators (which require air) to recharge their batteries.

Submarines traveling underwater encounter a drag force from the water they pass through. At low speeds, this force is proportional to their speed; the relationship is given by the simple formula

$$\vec F_{\rm {drag}} = -\gamma \vec v.$$

The number $\gamma$ (gamma) is a coefficient that tells you streamlined the submarine is -- how many newtons of drag you get per (meter per second) of velocity.

Consider a submarine with $\gamma = 80 \frac{\text{kN}}{\rm m/\rm s}$. This means that if the submarine is traveling at 1 m/s, it encounters a drag force of 80 kN; if it travels at 2 m/s, it encounters a drag force of 160 kN, etc.

This submarine has batteries that store $E=15$ GJ ($1.5 \times 10^{10}$ J) of energy, and electric motors that can convert it into mechanical work at a maximum of $P=2$ MW (megawatts). {\it (Assume anything else using the batteries doesn't use much energy.)}


{\it Note: You may calculate the following as numerical values, or may determine them in terms of $\gamma$, $E$, and $P$. All of the calculations here should be one or two lines. ``Dimensional analysis'' -- thinking about the units the various quantities are measured in and using this as a guide for what form the mathematics must take -- will help you greatly here. If you find yourself doing messy math, you are overthinking things.}


\begin{enumerate}
	\item For how much time can the submarine run at full power before its batteries are depleted? {\it (It travels at constant speed, so you don't need to worry about changes in its kinetic energy.)}
	

{\color{red}Power is the rate of converting energy. We can say: ``The submarine has 15 billion joules of energy in its batteries; it depletes that energy at a rate of 2 million joules per second.'' Mathematically, since 
	
	$$\text{power} = \frac{\text{energy}}{\text{time}}$$

we have 

$$t = \frac{E}{P}$$

This comes out to 7500 seconds or 125 minutes.

}
	
	
	\item What is the top speed $v_{\rm top}$ of the submarine while traveling underwater?
	
{	\color{red}
	
Here the submarine would slow to a stop because of water drag unless the motor continuously does positive work on it. Since it goes at a constant speed,

$$P_{\rm{motor}} + P_{\rm{drag}} = 0$$

where we expect the power from the motor to be positive and the power from drag to be negative. So what is the power from drag?

$$P_{\rm{drag}} = \vec F_{\rm{drag}} \cdot \vec v = F_{\rm{drag}} v = -(\gamma v) v = -\gamma v^2$$

So $P_{\rm{motor}} + P_{\rm{drag}} = 0$ implies $P_{\rm motor} = \gamma v^2$. Solving for $v$ we get 

$$v = \sqrt {\frac{P_{\rm motor}}{\gamma}}$$

This comes out to 5 m/s = 11.2 mph = 18 km/hr.
	
}

	
	\item What distance can the submarine travel at top speed before its batteries run out?

{\color{red} The motor does positive work on the submarine; it draws energy from the batteries to do this work. The batteries can supply a maximum energy $E$. The batteries will be drained once $W_{\rm{motor}} = E$. Since the positive work done by the motor is counteracting the negative work done by drag, we know $W_{\rm{motor}} = -W_{\rm{drag}}$.

So we need a formula for $W_{\rm drag}$. Since work is force times displacement, we have 

$$W_{\rm drag} = -F_{\rm drag} d = -\gamma v d$$

which means that 

$$E = \gamma v d \rightarrow d = \frac{E}{\gamma v}$$

Putting in our previous result for $v_{\rm max}$, this gives us $$d = \frac{E}{\sqrt{\gamma P}}$$

which comes out to 37.5 km.

}
	
	
	\item  Suppose that the submarine's crew wants to travel $d=100$ km underwater; perhaps they are passing under sea ice, or perhaps they need to hide.
	
	A sailor suggests that they don't have to run the engines at full power, so that they will travel slower but their batteries will last longer. They claim that this will let them travel further before their batteries are depleted. Will this work? If so, calculate the speed $v$ they must travel at so that they can go $d=100$ km without recharging their batteries. If not, explain why not.
	
{
	\color{red}

You may note that I solved the previous examples symbolically rather than numerically. This means that the relations I have derived are valid for {\it any} combination of parameters.

In the last part, we found a relation between the distance we can travel before the batteries are depleted, the energy in the batteries, the drag coefficient, and the velocity. Since $d = E / (\gamma v)$, we know that the distance traveled is inversely proportional to the velocity. This makes sense: going slower means you encounter less drag force, so the engine doesn't need to do as much work to balance it out.

Solving the last formula for $v$, we get 

$$v = \frac{E}{d \gamma}$$

This comes out to 1.875 m/s = 6.75 km/hr = 4.2 mph.

This is an interesting result, since it implies that you can go as far as you need to, as long as you go slow enough and take plenty of time. This isn't {\it quite} true, since eventually other demands for energy aboard the submarine start to matter: life support, running other machinery, etc. 

But it is definitely true that diesel-electric submarines can make their batteries last a lot longer by going much slower than their top speed.

}

\end{enumerate}

\end{document}
