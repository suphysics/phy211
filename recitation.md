---
layout: page
title: Recitation
permalink: recitation.html
category: top
use_math: true
---


* <a href="recitation/week1/recitation-units-motion.pdf">Week 1 Day 1: Mathematics with dimensions and units </a><br>
* <a href="recitation/week1/recitation-1D-motion-1.pdf">Week 1 Day 2: 1D motion, part 1</a>


* <a href="recitation/week2/recitation-1D-motion-2.pdf">Week 2 Day 1: 1D motion, part 2</a><br>
* <a href="recitation/week2/recitation-vectors.pdf">Week 2 Day 2: Vectors</a><br>

* <a href="recitation/week3/recitation-2D-motion.pdf">Week 3 Day 1: 2D motion</a><br>
* Week 3 Day 2 is the first group exam.

* <a href="recitation/week4/recitation-forces.pdf">Week 4 Day 2: Newton's laws of motion</a><br>

* <a href="recitation/week5/recitation-forces2.pdf">Week 5 Day 1: Friction; forces in two dimensions</a>
* <a href="recitation/week5/recitation-uniform-circular-motion-1.pdf">Week 5 Day 2: uniform circular motion</a>

* <a href="recitation/week6/recitation-coupled-objects.pdf">Week 6 Day 2: coupled objects and more practice</a>
* Week 6 Day 2 is the second group exam.

* <a href="recitation/week7/recitation-energy-1.pdf">Week 7 Day 2: the work-energy theorem</a>

* <a href="recitation/week8/recitation-potential-energy.pdf">Week 8 Day 1: More practice with the work-energy theorem; potential energy</a>
* <a href="recitation/week8/recitation-springs.pdf">Week 8 Day 2: The elastic force and elastic potential energy</a>

<!--





* <a href="recitation/week5/recitation-forces2.pdf">Week 5 Day 1: Friction; motion along a slope</a>
* <a href="recitation/week5/recitation-uniform-circular-motion-1.pdf">Week 5 Day 2: uniform circular motion</a>



* <a href="recitation/week6/recitation-gravity.pdf">Week 6 Day 1: Universal gravitation</a>
* Week 6 Day 2 is the second group exam.

* <a href="recitation/week7/recitation-energy-1.pdf">Week 7 Day 2: Work and energy</a>


* <a href="recitation/week8/recitation-potential-energy.pdf">Week 8 Day 1: Work and potential energy</a>
* <a href="recitation/week8/recitation-springs.pdf">Week 8 Day 2: Elasticity, potential energy, and power</a>

* <a href="recitation/week9/recitation-power.pdf">Week 9 Day 1: Power and energy</a>
* <a href="recitation/week9/recitation-momentum-1.pdf">Week 9 Day 2: Momentum</a>

* <a href="recitation/week10/recitation-momentum-2.pdf">Week 10 Day 1: Momentum in combination</a>
* Week 10 Day 2 is the third group exam.

* <a href="recitation/week11/recitation-torque-1.pdf">Week 11 Day 2: Introduction to torque</a>

* <a href="recitation/week12/recitation-static-equilibrium.pdf">Week 12 Day 1: Static equilibrium</a>
* <a href="recitation/week12/recitation-rotational-dynamics-1.pdf">Week 12 Day 2: Rotational dynamics</a>

* <a href="recitation/week13/recitation-translation-rotation.pdf">Week 13 Day 1: Coupled translation and rotation</a>
* <a href="recitation/week13/recitation-rotational-energy.pdf">Week 13 Day 2: Rptational energy</a>

* <a href="recitation/week14/recitation-transmission.pdf">Week 14 Day 1: Transmissions and gears</a> 
* Week 14 Day 2 is the fourth group exam.

-->

### Recitation Schedule

| Section Name |         Time        |            Room           |    TA   |
|:------------:|:-------------------:|:-------------------------:|:-------:|
| M009 | TTh  5:00 - 5:55 PM      | Heroy 114      |   Jed   |
| M025 | TTh  5:00 - 5:55 PM      | Physics 208    |  Hanieh |
| M017 | TTh  5:00 - 5:55 PM      | Physics 106    |   Luis  |
| M018 | TTh  6:30 - 7:25 PM      | Physics 106    |   Luis  |
| M010 | TTh  6:30 - 7:25 PM      | Heroy 114      | Gourang |
| M028 | TTh  6:30 - 7:25 PM      | Physics 208    |  Hanieh |
| M006 | WF   8:25 - 9:20 AM      | Physics 208    |   Adil  |
| M023 | WF   8:25 - 9:20 AM      | Physics 104N   |  Javad  |
| M011 | WF   8:25 - 9:20 AM      | Physics 106    |  Belal  |
| M004 | WF   9:30 - 10:25 AM     | Bowne Hall 104 |   Luis  |
| M026 | WF   9:30 - 10:25 AM     | Physics 208    |   Adil  |
| M012 | WF   9:30 - 10:25 AM     | Physics 106    | Srijana |
| M013 | WF   10:35 - 11:30 AM    | Physics 106    | Srijana |
| M022 | WF   10:35 - 11:30 AM    | Physics 208    |  Javad  |
| M024 | WF   10:35 - 11:30 AM    | Eggers 018     | Gourang |
| M014 | WF   11:40 AM - 12:35 PM | Physics 106    | Srijana |
| M019 | WF   11:40 AM - 12:35 PM | Eggers 032     |  Belal  |
| M020 | WF   11:40 AM - 12:35 PM | Heroy 113      | Hanieh  |
| M015 | WF   12:45 - 1:40 PM     | Physics 106    |   Adil  |
| M016 | WF   3:45 - 4:40 PM      | Physics 106    |  Javad  |
| M021 | WF   3:45 - 4:40 PM      | Physics 208    |  Belal  |
| M008 | WF   3:45 - 4:40 PM      | Physics 104N   | Gourang |
