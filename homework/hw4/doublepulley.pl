
$topy = 8;
$roofleft=-4;
$roofright=4;
$dx=0.1;


sub cable 
{
	my $w=0.1;
	my $x=shift;
	my $y1=shift;
	my $y2=shift;
	printf "line %e %e %e %e\n",$x-$w,$y1,$x-$w,$y2;
	printf "line %e %e %e %e\n",$x+$w,$y1,$x+$w,$y2;
	for ($y=$y1+0.1; $y<$y2; $y+=0.1)
	{
	  printf "line %e %e %e %e\n",$x-$w,$y-0.1,$x+$w,$y;
        }
  
}

sub cablebend
{
	$w=0.2;
	$x=shift;
	$y=shift;
	$r=shift;
	$r2=$r+$w;
	$t1=shift;
	$t2=shift;
	$t1 = $t1 * 3.14159/180;
	$t2 = $t2 * 3.14159/180;

	$ths=0.1/$r;
	for ($th=$t1; $th<$t2; $th += 0.1/$r)
	{
		printf "line %e %e %e %e\n",$x+$r*cos($th),$y+$r*sin($th),$x+$r*cos($th+$ths),$y+$r*sin($th+$ths);
		printf "line %e %e %e %e\n",$x+$r2*cos($th),$y+$r2*sin($th),$x+$r2*cos($th+$ths),$y+$r2*sin($th+$ths);
		printf "line %e %e %e %e\n",$x+$r*cos($th),$y+$r*sin($th),$x+$r2*cos($th+$ths),$y+$r2*sin($th+$ths);
	}
}


print "line $roofleft $topy $roofright $topy\n";
for ($x = $roofleft; $x<$roofright; $x+=$dx)
{
	printf "line $x $topy %e %e\n",$x + $dx, $topy + $dx;
}

$y1=2;
$y2=6;
$y3=3;

cable (-2, $y1, $y2);
cablebend(-1, $y2, 0.9, 0, 180);
cable (0, $y3, $y2);
cablebend(1, $y3, 0.9, 180, 360);
cable (2, $y3, $topy);

print "#cm 2\n";
for ($r=0; $r<=0.9; $r+=0.1)
{
printf "circ %e %e %e\n",-1, $y2, $r;
}
printf "line -1.1 $y2 -1.1 $topy\n";
printf "line -1.05 $y2 -1.05 $topy\n";
printf "line -1.0 $y2 -1.0 $topy\n";
printf "line -0.95 $y2 -0.95 $topy\n";
printf "line -0.9 $y2 -0.9 $topy\n";
print "#cm 1\n";
for ($r=0; $r<=0.9; $r+=0.1)
{
printf "circ %e %e %e\n", 1, $y3, $r;
}

print "#m 2 cm 0\n";
for ($y=-3; $y<$topy; $y++)
{
	printf "line $roofleft $y %e $y\n",$roofleft+1;
	printf "line $roofright $y %e $y\n",$roofright-1;
}

print "#m 1 cm 3\n";

printf "line %e %e %e %e\n",-2.5,$y1,-1.5,$y1;
printf "line %e %e %e %e\n",-2.5,$y1-1,-1.5,$y1-1;
printf "line %e %e %e %e\n",-2.5,$y1,-2.5,$y1-1;
printf "line %e %e %e %e\n",-1.5,$y1,-1.5,$y1-1;
printf "line %e %e %e %e\n",-2.4,$y1-0.1,-1.6,$y1-.1;
printf "line %e %e %e %e\n",-2.4,$y1-0.9,-1.6,$y1-.9;
printf "line %e %e %e %e\n",-2.4,$y1-0.1,-2.4,$y1-.9;
printf "line %e %e %e %e\n",-1.6,$y1-0.1,-1.6,$y1-.9;
printf "arrow -2 %e -2 %e\n",$y1-1,$y1-3;
