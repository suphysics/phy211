print "circ 0 0 0.5\n";
print "circ 0 0 8\n";

for ($dy=-0.08; $dy<0.08; $dy+=0.001)
{
	printf "line %e %e %e %e\n",-0.6,-8+$dy,0.5,-8+$dy;
	printf "line %e %e %e %e\n",-0.6,8+$dy,0.6,8+$dy;
	printf "line %e %e %e %e\n",-8.6,$dy,-7.4,$dy;
}
print "#cm 2\n";
print "stickfig 0 -8 0 -6.2\n";
print "stickfig 0 8 0 9.8\n";
print "stickfig -8 0 -8 1.8\n";

print "#cm 1\n";
print "arc 0 0 9.5 210 270\n";
print "arrowhead -4 -9.5 0 -9.5\n";
print "1 -9.5\"\\gw\"\n";
