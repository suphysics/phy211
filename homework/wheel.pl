for ($x=-6; $x<6; $x+=0.3)
{
    printf "line %e %e %e %e\n",$x,0,$x-0.3,-0.3;
}
print "line -6 0 6 0\n";

print "circ 0 3 3\n";
print "fillcirc 0 3 0.3\n";
print "arc -0.1 3 3 90 270\n";

for ($th=0; $th<2; $th+=0.0625)
{
    $th2=$th*3.14159;
    printf "line %e %e %e %e\n",0.3*sin($th2),3+0.3*cos($th2),3*sin($th2),3+3*cos($th2);
}

print "#cm 2\n";
print "arrow 0 3 5 3\n";
print "5.5 3 \"T\"\n";
