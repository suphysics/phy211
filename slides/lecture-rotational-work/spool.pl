$tiltfrac = 0.0;

$rin = 2;
$rout = 5;

$w=8;

$ths=3.14159/40;
for ($th=0; $th<3.14159; $th+=$ths)
{
    $yin=$rin*cos($th);
    $xin=sin($th)*$rin;
    $yout=$rout*cos($th);
    $xout=sin($th)*$rout;
printf "line %e %e %e %e\n",-$w+$tiltfrac*$xin, $yin,$tiltfrac*$xin, $yin;
printf "line %e %e %e %e\n",$w+$tiltfrac*$xout, $yout,$tiltfrac*$xout, $yout;
    $th2=$th+$ths;
    $yin2=$rin*cos($th+$ths);
    $xin2=sin($th+$ths)*$rin;
    $yout2=$rout*cos($th+$ths);
    $xout2=sin($th+$ths)*$rout;
    if ($th2 > 3.14159)
    {
      printf "line %e %e %e %e\n",-$w+$tiltfrac*$xin, $yin,-$w+$tiltfrac*$xin2, $yin2;
    }
    if ($th2 < 3.14159)
    {
      printf "line %e %e %e %e\n",$w+$tiltfrac*$xout, $yout,$w+$tiltfrac*$xout2, $yout2;
    }
    
    printf "line %e %e %e %e\n",$tiltfrac*$xin, $yin,$tiltfrac*$xin2, $yin2;
    
    if ($th1 < 3.14159 && $th2 < 3.14159)
    {
	#  printf "line %e %e %e %e\n",$tiltfrac*$xout, $yout,$tiltfrac*$xout2, $yout2;
    }
    
    $y2 = $yin;

   	#    printf "line %e %e %e %e\n",$tiltfrac*$xin, $yin,$tiltfrac*$xout, $yout;
}
print "line 0 $rout 0 -$rout\n";
print "line -$w $rin -$w -$rin\n";
print "line $w $rout $w -$rout\n";

$tf = 0.04;
$slop=0.1;
print "#cm 1\n";

  for ($l=0; $l<12; $l++)
  {
for ($th2=0; $th2<3.14159; $th2+=$ths)
{
      $th = $th2 + 2*3.14159*$l;
      $y1 = ($rin+$slop) * cos($th);
      $x1 = -5 + $th*$tf;
      $th = $th2 + 2*3.14159*$l + $ths;
      $y2 = ($rin+$slop) * cos($th);
      $x2 = -5 + $th*$tf;
      print "line $x1 $y1 $x2 $y2\n";
      


}
}
printf "line %e %e %e %e\n",$x2,$y2,$x2,-8;

print "#cm 2\n";

  for ($l=0; $l<12; $l++)
  {
for ($th2=0; $th2<3.14159; $th2+=$ths)
{
      $th = $th2 + 2*3.14159*$l;
      $y1 = ($rout+$slop) * cos($th);
      $x1 = 3 + $th*$tf;
      $th = $th2 + 2*3.14159*$l + $ths;
      $y2 = ($rout+$slop) * cos($th);
      $x2 = 3 + $th*$tf;
      print "line $x1 $y1 $x2 $y2\n";
      


}
}
printf "line %e %e %e %e\n",$x2,$y2,$x2,-8;
