$tilt = 0.2;
$r=5;
$first=1;

for ($th=-0.5; $th<3.14159*1.5; $th+=0.1)
{
    $th2=$th-0.1;

    printf "line %e %e %e %e\n",sin($th)*$r,cos($th)*$r*$tilt,sin($th2)*$r,cos($th2)*$r*$tilt;
    if ($first)
    {
    $x=sin($th)*$r;
    $y=cos($th)*$r*$tilt;
    $x2=sin($th2)*$r;
    $y2=cos($th2)*$r*$tilt;
    $dx=$x-$x2;
    $dy=$y-$y2;
    $first=0;
}
}
$tilt*=$r;
$x2=$x+$dx*5;
$y2=$y+$dy*5;
$x3=$x-$dx;
$y3=$y-$dy;

print "arrowhead $x2 $y2 $x3 $y3 \n";
print "-3.4 $tilt \"\\gw\"\n";

print "#cm 2\n";

print "line 0 5 0 -5\n";
print "arrowhead 0 2 0 5\n";
print "0 6 \"L\"\n";

