print "line -2 0 0 0\n";
print "line 0 0 0 -3\n";
print "line 0 -3 8 -3\n";
print "#cm 2\n";
print "stickfig -0.2 0 -0.2 1\n";


$th=50/180*3.14159;

$dt=0.09;


$v=2.4;

$vx=cos($th)*$v;
$vy=sin($th)*$v;
printf "arrow 0 0 %e %e\n",$vx,$vy;
printf "%e %e \"v\\sb0\\eb\n",$vx,$vy+0.5;
$g=1;
print "#cm 1\n";

for ($t=0; $y>=-3; $t+=$dt)
{
	$ox=$x;
	$oy=$y;

	$x+=$vx*$dt;
	$y+=$vy*$dt;
	$vy-=$dt*$g;

	print "line $ox $oy $x $y\n";

	$x+=$vx*$dt;
	$y+=$vy*$dt;
	$vy-=$dt*$g;
}

print "#cm 3 cs 0.7\n";
printf "line 0 -4 %e -4\n",$x*0.4;
printf "%e -4 \"d=?\"\n",$x*0.47;
printf "line %e -4 $x -4\n",$x*0.6;
print "line 0 -4.5 0 -3.5\n";
print "line $x -4.5 $x -3.5\n";

print "line -3 -3 -3 0\n";
print "line -3.3 -3 -2.8 -3\n";
print "line -3.3 0  -2.8 0 \n";
print "-3.3 -1.5 \"h\"\n";

