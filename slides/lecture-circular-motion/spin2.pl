while (1)
{
  $theta = $theta + 2 / 60;
  $x = sin($theta);
  $y = cos($theta);

  $x2 = 2*sin($theta/2);
  $y2 = 2*cos($theta/2);

  print "C 1 0.5 0.5\n";
  print "T -0.95 -0.92\n Radius 2    Angular velocity 1 rad/s    Tangential velocity 2 m/s\n";
  print "l3 0 0 0 $x2 $y2 0\n";
  print "ct3 1 $x2 $y2 0 0.1\n";\
  print "C 0.5 0.5 1\n";
  print "T -0.95 -0.85\n Radius 1    Angular velocity 2 rad/s    Tangential velocity 2 m/s\n";
  print "l3 0 0 0 $x $y 0\n";
  print "ct3 2 $x $y 0 0.1\n";\
  print "F\n";
}
