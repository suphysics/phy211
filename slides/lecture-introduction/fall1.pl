print "force2dframe\n";
open Fall,">falldata.txt";
$y=40;
$g=9.8;
$v=0;
for ($i=0; $y>0; $i++)
{
	$y+=$v*0.01;
	$v-=$g*0.01;
	printf "c3 0 $y 0 1\n";
	printf Fall "%e %e\n",0.01*$i,$y;
	printf "screenshot animations/fall-$i.png\n";
	print "F\n";
}
