\documentclass[10pt]{article}
\setlength\parindent{0pt}
\usepackage{fullpage}
\usepackage{graphicx}
\usepackage[margin=2cm]{geometry}

\usepackage{amsmath}
\setlength{\parskip}{4mm}

\begin{document}


\begin{center}
	\sc \Large Review of Kinematics in One and Two Dimensions
	
\end{center}

\section{Introduction}

The most fundamental question mechanics can answer is ``How will an object's position change as time passes?'', or equivalently ``What must I do to an object to make its position change in a certain way?'' 

The answer, which is the centerpiece of this course, is Newton's second law $\vec F = m \vec a$: this tells us that forces cause objects to accelerate.

So, if we are to understand how forces cause objects' position to change with time, we need to understand how acceleration influences the way things move. This is the subject of {\it kinematics} -- the relation between acceleration, velocity, and position.

We describe the way acceleration, velocity, and position vary with time as mathematical functions:

\begin{itemize}
	\item $\vec s(t)$ -- how the position vector varies with time
	\begin{itemize}
		\item $x(t)$ -- how the $x-$component of position varies with time
		\item $y(t)$ -- how the $y-$component of position varies with time
	\end{itemize}
	\item $\vec v(t)$ -- how the velocity vector varies with time
\begin{itemize}
	\item $v_x(t)$ -- how the $x-$component of velocity varies with time
	\item $v_y(t)$ -- how the $y-$component of velocity varies with time
\end{itemize}
	\item $\vec a(t)$ -- how the position vector varies with time
\begin{itemize}
	\item $a_x(t)$ -- how the $x-$component of acceleration varies with time
	\item $a_y(t)$ -- how the $y-$component of acceleration varies with time
\end{itemize}
\end{itemize}

These functions can be represented graphically (e.g. a parabola) or as algebraic functions (e.g. $x(t) = \frac{1}{2}gt^2$).


\section{Coordinate systems}

We must choose a physical point to be ``position zero'', along with what directions correspond to ``+x'' and ``+y''. (For instance, you might choose the position x-direction to be to the right, and the positive y-direction to be upward.)
It is important that this be consistent. 

Any choice will work, as long as you are consistent in applying your coordinate system.

The best way to think clearly about this is to sketch a cartoon of the motion being described, label a point as your origin (abbreviation $\mathcal O$), then draw two perpendicular arrows in the $+x$ and $+y$ directions.

This should be the first thing you do in analyzing any situation, particularly if something is moving in two dimensions.

\section{Acceleration, velocity, and position}

\subsection{Going from position to velocity to acceleration: derivatives}

Acceleration is the rate of change of velocity, and velocity is the rate of change of position.

A rate of change is called a {\it derivative} in calculus. This means that acceleration is the derivative of velocity; velocity is the derivative of position.

When these things are graphed, the rate of change of a function is the slope of its graph. This means that:

\begin{itemize}
	\item The {\it value} of the acceleration graph is the {\it slope} of the velocity graph at any point in time
	\item The {\it value} of the velocity graph is the {\it slope} of the position graph at any point in time
\end{itemize}

Some examples:
\begin{itemize}
	\item If acceleration is constant, then the velocity vs. time graph will be a straight line whose slope is equal to the acceleration
	\item If acceleration is zero, then velocity will be constant, and the velocity vs. time graph will be a flat line (slope zero)
	\item If velocity is constant, then the position vs. time graph will be a straight line whose slope is equal to the velocity
\end{itemize}

\subsection{Going from acceleration to velocity to position: integrals}

The opposite of a {\it derivative} in calculus is an {\it integral}. The best way to think of an integral intuitively is as the accumulated effect of something that persists over time. A large velocity that persists over a short time will cause a change in position equal to a small velocity that persists over a long time.

Since integrals are the reverse of derivatives, this means:

\begin{itemize}
	\item The change in an object's velocity is equal to the integral of its acceleration
	\item The change in an object's position is equal to the integral of its position
\end{itemize}

The graphical interpretation of an integral is the {\it area under the curve} when it is graphed. 

\begin{itemize}
	\item The change in an object's velocity is equal to the area under its acceleration vs. time curve
	\item The change in an object's position is equal to the area under its velocity vs. time curve
\end{itemize}

\subsection{Constant acceleration kinematics}

A common situation is where an object's acceleration is constant. This happens when the force acting on it is constant, such as when it is moving only under the influence of gravity. We would like to determine its position vs. time function in this case.

\begin{minipage}{0.6\textwidth}
If an object's acceleration is constant, then its acceleration vs. time function is just a constant: $$a(t) = a.$$

Its graph is also just a flat line, as shown to the right.
\end{minipage}
\hspace{0.02\textwidth}
\begin{minipage}{0.38\textwidth}
	\includegraphics[width=\textwidth]{slides/lecture-calculus/area-under-a-crop.pdf}
\end{minipage}

The area under this curve is just $at$, which gives the change in velocity. This means that over the time $t$, the velocity will change from its initial value $v_0$ to a final value of $v_0 + at$.

We can look at it from the reverse direction too. Since the value of acceleration is the slope of velocity, the velocity vs. time graph will be a straight line with a slope of $a$. We know that the equation of a line is in general $y=mx+b$, where $b$ is the initial value, $x$ is the horizontal coordinate/independent variable, $y$ is the vertical coordinate/dependent variable, and $m$ is the slope. In this case we want to put in the variables specific to {\it our} line, giving us

\begin{align}
v(t) = at + v_0 & \hspace{1in} \text{(velocity vs. time for constant acceleration)}
\end{align}


\begin{minipage}{0.6\textwidth}
This function is graphed to the right. The velocity begins at the initial value $v_0$ and increases to the value $v_0 + at$ over a time $t$. I have shaded the region under this curve in two different colors to make it easy to compute its area; it has the shape of a triangle sitting on top of a rectangle.

\begin{itemize}
	\item Area of triangle = $\frac{1}{2}(base)(height) = \frac{1}{2}(t)(at) = \frac{1}{2}at^2$
	\item Area of rectangle = $(width)(height) = v_0 t$
\end{itemize}

\end{minipage}
\hspace{0.02\textwidth}
\begin{minipage}{0.38\textwidth}
	\includegraphics[width=\textwidth]{slides/lecture-calculus/area-under-v-full-crop.pdf}
\end{minipage}

Since the change in position is the area under the velocity vs. time graph, this means that over time $t$, the position will change from its initial value $x_0$ to a final value of $x_0 + \frac{1}{2}at^2 + v_0 t$.

This gives us:

\begin{align}
x(t) = \frac{1}{2}at^2 + v_0 t + x_0 & \hspace{1in} \text{(position vs. time for constant acceleration)}
\end{align}



\end{document}


