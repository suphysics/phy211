\documentclass[12pt]{article}
\setlength\parindent{0pt}
\usepackage{fullpage}
\usepackage[margin=0.5in]{geometry}
\usepackage{amsmath}
\usepackage{pdflscape}
\usepackage{xcolor}
\usepackage{graphicx}
\setlength{\parskip}{4mm}
\def\LL{\left\langle}   % left angle bracket
\def\RR{\right\rangle}  % right angle bracket
\def\LP{\left(}         % left parenthesis
\def\RP{\right)}        % right parenthesis
\def\LB{\left\{}        % left curly bracket
\def\RB{\right\}}       % right curly bracket
\def\PAR#1#2{ {{\partial #1}\over{\partial #2}} }
\def\PARTWO#1#2{ {{\partial^2 #1}\over{\partial #2}^2} }
\def\PARTWOMIX#1#2#3{ {{\partial^2 #1}\over{\partial #2 \partial #3}} }
\newcommand{\BE}{\begin{displaymath}}
\newcommand{\EE}{\end{displaymath}}
\newcommand{\BNE}{\begin{equation}}
\newcommand{\ENE}{\end{equation}}
\newcommand{\BEA}{\begin{eqnarray}}
\newcommand{\EEA}{\nonumber\end{eqnarray}}
\newcommand{\EL}{\nonumber\\}
\newcommand{\la}[1]{\label{#1}}
\newcommand{\ie}{{\em i.e.\ }}
\newcommand{\eg}{{\em e.\,g.\ }}
\newcommand{\cf}{cf.\ }
\newcommand{\etc}{etc.\ }
\newcommand{\Tr}{{\rm tr}}
\newcommand{\etal}{{\it et al.}}
\newcommand{\OL}[1]{\overline{#1}\ } % overline
\newcommand{\OLL}[1]{\overline{\overline{#1}}\ } % double overline
\newcommand{\OON}{\frac{1}{N}} % "one over N"
\newcommand{\OOX}[1]{\frac{1}{#1}} % "one over X"

\pagenumbering{gobble}

\begin{document}
\Large
\centerline{\sc{Recitation Exercises - Static and Dynamic Equilibrium}}
\normalsize
\centerline{\sc{Week 12, Day 1}}

\begin{center}
	\large Question 1: pulling a table over
\end{center}

	A table is constructed so that its height is two-thirds of its width. The legs are quite light; 
	to a good approximation, all of the table's mass is in its legs. The legs of the table are located at its 
	ends and a rope is tied to one side; a person pulls on the rope with gradually increasing tension.
	\medskip
	
	Assume that the coefficient of friction between the legs and the floor is quite large so that the table does not slide.
	\medskip
	
	In this exercise, you'll determine the maximum tension $T$ before the table begins to tip over. You can do this with the following steps:
	
	\begin{enumerate}
		\item Here is a large image of the table. Draw an extended force diagram on top of the image, showing both where all the forces act and which direction they point. Think carefully about what forces act on the legs of the table at the points where they touch the ground.
		
		
		
		\vspace{0.5in}
		
					\includegraphics[width=0.9\textwidth]{table-crop.pdf}
		{\color{red}
			
			The forces are: 
			
			\begin{itemize}
				\item Tension, where it is drawn, to the right
				\item Weight, at the center of the top plane of the table, down
				\item Normal forces at the base of each leg, pointing upward
				\item Frictional forces at the base of each leg, pointing leftward
			\end{itemize}
					}
					\newpage
					\item Imagine pulling the rope harder and harder until the table begins to tip, and imagine how the forces you have drawn change as this happens.  What is true mathematically about these forces at the moment when the table begins to tip?
					
					{\color{red}The table begins to tip when its back leg (left leg) comes off the ground. This means that the table tips when the left leg's normal force (however they have chosen to label it) becomes zero. (Since $F_{\rm fric} = \mu_s F_N$, this also means the left leg's frictional force is also zero.)}
					
					\item You would like to determine the maximum tension $T$ before the table tips. Given this, and your finding in the last question, which point should you choose as your pivot? {\it (Remember the pivot should be at the location of a force you do not know about and do not care about.)} Mark this point on your force diagram.
					
					{\color{red} Life will be much easier if we choose the base of the right leg to be our pivot. There is a normal force there that we don't care about and don't know. By choosing the pivot there, we make that force not apply a torque so we don't really care about how big it is.}
					
				
					
					\item There are three methods for determining the torque applied by a force:
					
					\begin{enumerate}
						\item The component of the force perpendicular to the radius, multiplied by the radius
						\item The force, multiplied by the component of the radius that is perpendicular to the force
						\item The force, times the radius, times the sine of the angle between them
					\end{enumerate}
					
					One of these methods makes it {\bf much} easier to compute the torque from the weight of the table. Which one is it?
					
					{\color{red} {\bf This is a big deal for the students to understand.} The pivot is at the base of the right leg. Calculating $r$ is hard here (it's $\sqrt{\frac{w^2}{4} + \frac{4w^2}{9}}$ by the Pythagorean theorem). Then, once you do that, you have to figure out the angle between that diagonal radius and the vertical/horizontal. But we can avoid that by choosing the second method: here $r_\perp$ is just half the width and the weight force is just $mg$, so the torque due to gravity is
						
						$$\tau_{\rm {grav}} = \frac{1}{2}Wmg$$
						
						where $W$ is the width of the table. Doing it the other way will result in some godawful trig stuff that all cancels in the end.
					}
					
					
					
					\item Determine the torque from each of the forces you've drawn on your force diagram at the point when the table begins to tip.
					
					{\color{red}Here the normal force from the right leg applies no torque (it's at the pivot) and there is no normal force at the left leg (it's tipping). The only other force is the tension, where the torque is 
						
						$$\tau_{\rm{T}} = -\frac{2}{3}WT.$$
					}
					
					
					\item What is the maximum value of the tension $T$ before the table tips?
{\color{red}The table begins to tip when the sum of the torques is zero. So
	
	$$\tau_{\rm {grav}}  + \tau_{\rm {T}} = 0 \rightarrow \frac{2}{3}WT = \frac{1}{2}Wmg \rightarrow T = \frac{3}{4}mg.$$
}
\vspace{2in}
	\end{enumerate}

	
	\begin{center}\large Exercise 2: a balancing act\end{center}
		
		A horizontal meter stick of mass $m$ is resting on two supports, one at the 30 cm mark and one at the 60 cm mark. (The lower numbers are on the left and the larger numbers are on the right.)
		
		\begin{enumerate}
			\item Determine the force on both supports. (Note that since the meter stick does not move, we know that {\it both} $\sum F = 0$ and $\sum \tau = 0$. These two equations together will give you enough information to solve for two unknowns. You could also construct $\sum \tau = 0$ around two different pivots, which will also give you two equations!)

{\color{red}In this case they will need to draw the extended force diagram as before. There are three forces:
	
	\begin{itemize}
		\item $F_{N1}$ pointing up at the 30cm mark
		\item $F_{N2}$ pointing up at the 60cm mark
		\item $mg$ pointing down at the 50cm mark
	\end{itemize}
			
			In this case there is no one ``obvious'' choice for the pivot. They'll need to construct $\sum \tau = 0$ around any pivot point and then either $\sum F = 0$ or $\sum \tau = 0$ around another pivot point. This will give them a system of two equations that they can use to solve for $F_{N1}$ and $F_{N2}.$			
		}
			\vspace{4in}
			
			\newpage
			
		
		\item Now, one of the Physics 211 pet frogs escapes from his bucket and jumps on the meter stick. This is one of our smaller frogs, so his mass is only half as much as the meter stick's mass.
		
		Determine the furthest point to the left (toward the 0 cm mark) and the furthest point to the right (toward the 100 cm mark) that the frog can sit while the meter stick remains balanced.
		
		Note that now you have a force with an unknown position (the frog's weight). That's okay! 
		You can introduce a variable $x$ for the unknown position; then you can write down a mathematical expression for the torque that it applies around any pivot that will have $x$ in it. 
		
		Then the condition $\sum \tau = 0$ will let you solve for $x$.
		
		{\color{red} There are a bunch of different ways to define $x$, but the simplest is to choose it as the actual meter stick number the frog sits on. (This is why we do this with a meter stick -- it's easier to visualize.)
		
		Let's use $m$ as the mass of the frog and $2m$ as the mass of the meter stick to avoid fractions. Then we have two separate problems to solve:
		
		\begin{enumerate}
			\item When the frog is too far to the right, the meter stick will tip over when $F_{N1}$ becomes 0. We can choose the pivot at the location of $F_{N2}$ (60 cm mark). This means that we have:
			
			$$\tau_{\rm frog} = -(x - 60 {\rm cm})mg$$
			$$\tau_{\rm stick} = (10 {\rm {cm})} (2mg)$$
			
			Adding these together and setting to zero gives $x = 80$ cm as the rightmost position of the frog.
			
			\item When the frog is too far to the left, the meter stick will tip over when $F_{N2}$ becomes 0. We can choose the pivot at the location of $F_{N1}$ (30 cm mark). This means that we have:
			
			$$\tau_{\rm frog} = (30 {\rm cm} - x)mg$$
			$$\tau_{\rm stick} = -(20 {\rm {cm})} (2mg)$$
			
			Adding these together and setting to zero gives $x = -10$ cm as the leftmost position of the frog. But the meter stick doesn't have a ``-10 cm'' marking -- this means the frog can hop all the way to the left side without the meter stick falling.
			
		\end{enumerate}
		
		}
		
		\end{enumerate}
		
		
	
	
	
\end{document}
