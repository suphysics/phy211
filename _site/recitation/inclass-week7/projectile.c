#include <stdio.h>
#include <math.h>
#include "vector.h"


void lin(vector v1, vector v2)
{
    printf("l3 %e %e %e %e %e %e\n",v1.x,v1.y,v1.z,v2.x,v2.y,v2.z);
}


void arrow (vector o, vector v, double r, double g, double b)
{
  printf("C %f %f %f\n",r,g,b);
  lin(o,o+v);
  // generate two vectors orthogonal to v

  vector x,y;
  x.x=12; x.y=36; x.z=107;

  y=v^x; y=y/norm(y)*norm(v);
  x=v^y; x=x/norm(x)*norm(v);


  for (double th=0; th<2*M_PI; th+=0.5)
  {
    lin(o+v, o+v*0.8+sin(th)*x*0.1+cos(th)*0.1*y);
    x=x;
  }
}

int main(void)
{
    double h=15;
    double th1=-10 * M_PI/180;
    double th2=85 * M_PI/180;
    double v0=20;

    vector s1=vector(0,h,0);
    vector s2=s1;
    vector v1=vector(v0*cos(th1), v0*sin(th1), 0);
    vector v2=vector(v0*cos(th2), v0*sin(th2), 0);

    double dt=5e-6;

    while (1)
    {
	printf("C 1 1 1\n");
	vector base(0,0,0);
	vector top(0,h,0);
	vector left(-h*3,h,0);
	vector right(h*6,0,0);

	lin(base,top);
	lin(top, left);
	lin(base,right);

	printf("C 1 0.5 0.5\n");
	printf("ct3 1 %e %e %e 1\n",s1.x,s1.y,s1.z);
	printf("T -0.9 -0.8\nMagnitude of velocity 1 = %.4f\n",norm(v1));
	printf("T 0.0 -0.8\nVelocity 1 = (%.4f, %.4f)\n",v1.x,v1.y);
	printf("C 0.2 0.2 1\n");
	printf("ct3 2 %e %e %e 1\n",s2.x,s2.y,s2.z);
	printf("T -0.9 -0.9\nMagnitude of velocity 2 = %.4f\n",norm(v2));
	printf("T 0.0 -0.9\nVelocity 2 = (%.4f, %.4f)\n",v2.x,v2.y);

	for (int i=0; i<1000; i++)
	{
	if (s1.y > 0)
	{
	    s1 = s1 + v1 * dt/2;
	    v1.y -= 10 * dt;
	    s1 = s1 + v1 * dt/2;
	}

	if (s2.y > 0)
	{
            s2 = s2 + v2 * dt/2;
	    v2.y -= 10 * dt;
            s2 = s2 + v2 * dt/2;
	}
	}


	arrow (s1, v1*0.5, 1, 0.3, 0.3);
	arrow (s2, v2*0.5, 0.3, 0.3, 1);
	
	printf("F\n");
    }
}

