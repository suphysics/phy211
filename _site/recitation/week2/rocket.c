#include <stdio.h>

int main(void)
{
	double dt=0.01,t;
	double y=0, v=0;
	FILE *accfile = fopen("acceleration", "w");
	FILE *velfile = fopen("velocity", "w");
	FILE *posfile = fopen("position", "w");
	fprintf(accfile,"#y -20 20 h 0.5\n");
	fprintf(velfile,"#h 0.5\n");
	fprintf(posfile,"#h 0.5\n");
	for (t=0; y>=0; t+=dt)
	{
		if (t<10)
			fprintf(accfile, "%e %e\n",t,15.0);
		else
			fprintf(accfile, "%e %e\n",t,-10.0);

		fprintf(velfile, "%e %e\n",t,v);
		fprintf(posfile, "%e %e\n",t,y);
		
		y = y + v * dt/2;
		if (t<10) v+=15*dt; else v-=10*dt;
		y = y + v * dt/2;
	}
}



