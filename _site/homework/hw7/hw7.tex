
\documentclass[12pt]{article}
\setlength\parindent{0pt}
\usepackage{fullpage}
\usepackage[margin=0.5in]{geometry}
\usepackage{amsmath}
\usepackage{graphicx}
\setlength{\parskip}{4mm}
\def\LL{\left\langle}   % left angle bracket
\def\RR{\right\rangle}  % right angle bracket
\def\LP{\left(}         % left parenthesis
\def\RP{\right)}        % right parenthesis
\def\LB{\left\{}        % left curly bracket
\def\RB{\right\}}       % right curly bracket
\def\PAR#1#2{ {{\partial #1}\over{\partial #2}} }
\def\PARTWO#1#2{ {{\partial^2 #1}\over{\partial #2}^2} }
\def\PARTWOMIX#1#2#3{ {{\partial^2 #1}\over{\partial #2 \partial #3}} }
\newcommand{\BE}{\begin{displaymath}}
\newcommand{\EE}{\end{displaymath}}
\newcommand{\BNE}{\begin{equation}}
\newcommand{\ENE}{\end{equation}}
\newcommand{\BEA}{\begin{eqnarray}}
\newcommand{\EEA}{\nonumber\end{eqnarray}}
\newcommand{\EL}{\nonumber\\}
\newcommand{\la}[1]{\label{#1}}
\newcommand{\ie}{{\em i.e.\ }}
\newcommand{\eg}{{\em e.\,g.\ }}
\newcommand{\cf}{cf.\ }
\newcommand{\etc}{etc.\ }
\newcommand{\Tr}{{\rm tr}}
\newcommand{\etal}{{\it et al.}}
\newcommand{\OL}[1]{\overline{#1}\ } % overline
\newcommand{\OLL}[1]{\overline{\overline{#1}}\ } % double overline
\newcommand{\OON}{\frac{1}{N}} % "one over N"
\newcommand{\OOX}[1]{\frac{1}{#1}} % "one over X"



\begin{document}
\pagenumbering{gobble}
\Large
\centerline{\sc{Homework 7}}

\normalsize
\centerline{\sc{Due Friday, March 31}}

\begin{enumerate}


\item{In the classic computer game {\it Portal}, the player is asked to solve
	puzzles with the use of a ``portal device''.
	

\begin{minipage}{0.4\textwidth}
	This device can create two connected portals
	on, for example, the floor and a wall. An object entering one portal with speed $v$
	will exit the other with the same speed. 
	The game's narrator helpfully explains that ``Forward momentum, a product of mass
	and velocity, is conserved between portals. In layman's terms: {\bf speedy thing
		goes in; speedy thing comes out.}''
	\end{minipage}
	\begin{minipage}{0.5\textwidth}
		\hspace{0.1\textwidth}
	\includegraphics[width=0.8\textwidth]{Portal_physics-2.png}
\end{minipage}
	\begin{enumerate}
	\item Is this an accurate statement of the law of conservation of momentum? Does such a device conserve momentum, as the narrator states? If not, why not?
	
	\item Does this portal device conserve kinetic energy?
	
	\item Does it conserve {\it total} energy (kinetic plus potential)?
	\end{enumerate}
	
}

\bigskip


\item On a cold winter day, you might rub your hands together to warm them up. The kinetic friction as you rub your hands together does negative work on your hands.
The law of conservation of energy says that energy is never lost, only converted from one form to another; in this case,
this energy is dissipated as heat. 
\begin{enumerate}
\item Estimate the power (in watts) produced by this process. You will need to make quite a few estimates here involving the
parameters of rubbing your hands together; tell me what they are in your solution. (There is no unique right answer here; we are interested in your thought process.)
\item It requires about four joules of added heat to raise the temperature of one gram of tissue by one degree Celsius (1.8 degrees Fahrenheit). 
Is rubbing your hands together an effective way to keep your hands warm?
What about your whole body? Again, tell me what approximations and assumptions you make here. 
\end{enumerate}

\bigskip\newpage

\item A slingshot is a device that uses elasticity to launch small rocks through the air. The user places a rock against an elastic band, pulls the rock backwards (stretching the elastic), then releases the rock; the elastic force propels it forwards at high speed.

Suppose a child stands at the edge of a cliff with a height $h$ and launches a rock of mass $m$ into the air. {\it (You do not know the angle.)} Their slingshot is made using an elastic band of spring constant $k$, and the child draws the slingshot back a distance $b$.

\begin{enumerate} 
	\item How fast will the rock be going when it leaves the slingshot in terms of $m$, $b$, and $k$?
	\item How fast will the rock be traveling when it hits the ground in terms of $m$, $b$, $k$, $g$, and $h$?
	\item Explain why you don't need to know the launch angle to determine the speed that the rock hits the ground with.
	\item Explain why you {\it do} need to know the launch angle to determine the {\it place} where the rock hits the ground, and why you can't use energy methods to figure this out.
\end{enumerate}



\item The top end of a spring with spring constant $k$ is attached to the ceiling; a mass $m$ hangs from the bottom (initially at rest). 

\begin{enumerate}
	\item How far below the spring's equilibrium point does the mass hang?
	\item A person attaches a second mass $m$ to the spring and releases it. When the mass is added, the extra weight stretches the spring out further,
	falling down an additional distance before the elastic force pulls it back up.
	
	How far below the spring's equilibrium point do the masses fall before they start to come back up?
	
	\item The masses bounce up and down for a while before eventually
	coming to rest (due to air drag, friction, and the like).
	Once they come to rest, how far below the equilibrium point
	will they be located?
\end{enumerate}

{\it Hint: This problem is surprisingly nuanced. In each case, think very carefully about whether the net \rm{force} on the masses \it or their \rm{velocity} \it is zero.}

\bigskip

\item A professor from Syracuse University's Department of Mad Science sees the Physics Department's rocket-powered car and decides to improve it. They increase the rocket thrust to $F_T$, put skis on the bottom (to suit the Syracuse winters), and take it out to the frozen surface of Lake Onondaga to test it. The rocket car along with the mad scientist have a mass of $m$. 

However, they accidentally leave their calculator in radian mode while assembling it, so the rocket instead exhausts its gas at an angle $\theta$ below the horizontal. (This means that the thrust force points forward and upward.)

The coefficient of kinetic friction between the ice and the skis is $\mu_k$.

They fire the rocket and travel forward along the ice. After traveling a distance $d$, they confirm that this will suffice for their mad-scientific purposes and shut down the rocket; they coast a further distance $b$ before coming to a stop.

\begin{enumerate}
	\item Write an expression for the work done by the force of friction during the entire motion.
	\item Write an expression for the work done by the thrust force during the entire motion.
	\item Show that the distance the sled coasts is 
	
	$$b=\frac{F_T d \cos \theta - \mu mgd + \mu d F_T \sin \theta}{\mu mg}.$$
	
\end{enumerate}

\item A girl of mass $m_c = 40$ kg is sitting in a swing; the swing is attached by a rope of length $L=3$ m to a tree branch over her head.

The girl's very excited dog of mass 15 kg, happy to see his human, runs and jumps into her lap. This makes the girl (and dog) swing backwards.

The swing swings up into the air to a maximum angle of $15^\circ$ before coming back down.

\begin{enumerate}
	\item Draw three cartoons: one just before the dog jumps into her lap, one just after, and one when the swing has reached its maximum $15^\circ$ angle.
	\item What physical principle can you use to relate the velocity of the dog in the first cartoon to the velocity of the dog and girl in the second cartoon?
	\item What physical principle can you use to relate the velocity of the dog and girl in the second cartoon to the maximum swing angle in the third cartoon?
\item How fast was the dog running before he jumped into her lap? 
\end{enumerate}

\bigskip






%
%\item A person of mass 50 kg is ice-skating on a frozen lake with his dog Kibeth, who has a mass of 15 kg. He is skating due north at 3 m/s.
%
%Kibeth realizes that he's carrying snacks in his pocket, and would like one for herself. (Or maybe she is just being friendly!) She runs after him and tackles him from behind and the side, knocking him down. The two of them collapse on the ice and begin to slide, as Kibeth tries to get the treats out of his pocket; they are moving at an angle 20 degrees west of north at 4 m/s.
%
%What was Kibeth's velocity before she tackled him? {\it (Remember velocity is a vector.)}
%
%\bigskip
%
%\item Suppose that two small spacecraft, each with a mass of 2000 kg, are drifting next to each other. One of them has an astronaut on board; with her equipment, she has a mass of 200 kg. 
%
%Both craft are moving in the $x-$direction at a velocity of $\vec v_i = (0.5~\rm m/\rm s, 0)$. An astronaut wants to travel from one to the other. She pushes off of one spacecraft and jumps onto the other craft; as she floats through the air, she travels in the $y-$direction with a velocity of $\vec v_a = (0, 1~\rm m/\rm s)$. Note that when she moves through the air, she is moving {\it only} in the y-direction.
%
%
%\begin{enumerate}
%	\item What will the velocity of the first spacecraft be after she jumps off of it? {\it (Velocity is a vector; you can give its components.)}
%	
%	
%	\item What will the velocity of the second spacecraft be after she lands on it?
%	
%	
%	\item Explain in words why the $y-$components of their final velocities are {\it almost} equal and opposite, but are not quite the same magnitude.
%	
%	\item Explain in words why the $x-$components of their final velocities are {\it almost} equal. 
%\end{enumerate}

%\bigskip
%
%\item A forensics laboratory is investigating a crime and wants to measure how fast a particular gun shoots bullets. They don't have access to high-speed photography. All they have a clay block sitting on a table.
%
%One of the detectives says ``Hey, we can figure it out using this! We'll set the block on a table and shoot a bullet into the block; the faster the bullet is going, the further the block will slide before it comes to a stop!''
%
%Another detective says ``Yes, we can do that! In addition to how far the block slides on the table, we'll need to measure two things about the block and one thing about the bullet in order to figure it out.''
%
%\begin{enumerate}
%	\item What things must they measure to conduct the experiment?
%	\item Determine a formula for the speed of the bullet in terms of the other things you measured.
%\end{enumerate}

\item Drivers approaching the city of Denver, Colorado from the west travel on Interstate 70 as they descend from the Rocky Mountains (a tunnel at 3400 meters above sea level) into the city. 
\medskip

\begin{minipage}{3in}
Consider a section of this road that is 23 km long; drivers descend 800 meters in this distance. In this problem, you will consider the difficulties faced by truck drivers as they descend this section of road into Denver; as an example, consider a truck with a mass of 30,000 kg.

\end{minipage}
\begin{minipage}{4in}
\begin{center}
\includegraphics[width=3in]{trucker-sign.jpg}

\tiny \it Photo: Hart Van Denburg / Colorado Public Radio

\end{center}
\end{minipage}
\medskip



\begin{enumerate}
	\item Assuming a completely ideal situation in which the truck rolled freely down the slope with no interference from friction, how fast would the truck be traveling at the bottom of the slope. Convert this into km/hr or miles/hour; is this a reasonable thing for a truck driver to do?
	
	\item Thankfully, trucks are equipped with brakes which convert kinetic energy into heat through friction. This heat is then dissipated into the atmosphere. If a truck wants to maintain a constant speed down the hill, how much energy must its brakes absorb in total? (Compare this to the 270 MJ stored in the batteries of last week's Tesla.)
	
	\item Suppose a truck's brakes can dissipate heat at a rate of 250 kW. (If the driver tries to go faster than that, their brakes will heat up and lose effectiveness.) How fast could our driver go down this road safely? 
	
\end{enumerate}


\bigskip

\item A cyclist along with their bicycle has a mass of $m=60$ kg. They are capable of generating a power $P=150$ watts for a long time. (This means that they can do work on the pedals of their bicycle at a rate of 150 watts, and thus the bicycle's traction can do work on the bicycle at the same rate.)

There is a hill leading from Jamesville to Manlius that is elevated at an angle of around $6^\circ$. When going up a hill such as this one, gravity is the main force doing negative work on a cyclist; air resistance doesn't matter much.

How fast could our cyclist ride up the hill?


\bigskip\newpage

\item Most submarines throughout history have propelled themselves using electric batteries underwater. This limits their endurance when operating underwater since the same mass of batteries does not store as much energy as diesel. When surfaced, they use diesel generators (which require air) to recharge their batteries. 

Submarines traveling underwater encounter a drag force from the water they pass through. At low speeds, this force is proportional to their speed; the relationship is given by the simple formula

$$\vec F_{\rm {drag}} = \gamma \vec v,$$

where the number $\gamma$ (gamma) is a ``drag coefficient'' that tells you streamlined the submarine is.

Consider a submarine with $\gamma = 80 \frac{\text{kN}}{\rm m/\rm s}$. This means that if the submarine is traveling at 1 m/s, it encounters a drag force of 80 kN; if it travels at 2 m/s, it encounters a drag force of 160 kN, etc. 

This submarine has batteries that store $E=15$ GJ ($1.5 \times 10^{10}$ J) of energy, and electric motors that can convert it into mechanical work at a maximum of $P=2$ MW. {\it (Assume anything else using the batteries doesn't use much energy.)}

\begin{enumerate}
	\item For how much time can the submarine run at full power before its batteries are depleted? {\it (It travels at constant speed, so you don't need to worry about changes in its kinetic energy.)}
	\item What is the top speed $v_{\rm top}$ of the submarine while traveling underwater?
	\item What distance can the submarine travel at top speed before its batteries run out?
	\item  Suppose that the submarine's crew wants to travel $d=100$ km underwater; perhaps they are passing under sea ice, or perhaps they need to hide.
	
	      A sailor suggests that they don't have to run the engines at full power, so that they will travel slower but their batteries will last longer. They claim that this will let them travel further before their batteries are depleted. Will this work? If so, calculate the speed $v$ they must travel at so that they can go $d=100$ km without recharging their batteries. If not, explain why not.
\end{enumerate}

{\it Note: You may calculate the following as numerical values, or may determine them in terms of $\gamma$, $E$, and $P$. All of the calculations here should be one or two lines. ``Dimensional analysis'' -- thinking about the units the various quantities are measured in and using this as a guide for what form the mathematics must take -- will help you greatly here. If you find yourself doing messy math, you are overthinking things.}


 

\end{enumerate}
\end{document}
