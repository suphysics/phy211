$dx=0.1;
$th=20;
$thr=20*3.14159/180;
$L=6;

print "line -3 0 0 0\n";

for ($x=0; $x<8; $x+=$dx)
{
    $y=$x * sin($thr)/cos($thr);
    printf "line $x $y %e %e\n",$x+0.1,$y-0.1;
    if ($x < 3) {printf "line -$x 0 %e %e\n",-$x-0.1,-0.1;}
}
printf "line 0 0 %e %e\n",9*cos($thr),9*sin($thr);

print "#cm 3 m 2\nline 0 0 8 0\n";
print "arc 0 0 2 0 $th\n";
print "#m 1 cs 1\n 2.5 0.4 \"\\gh\"\n";

$os1=0.3;
$os2=0.2;
print "#cm 2\n";
print "fillcirc 0 $os1 0.1\n";
printf "-0.5 %e \"A\"\n",$os1;
printf "line 0 $os1 %e %e\n",cos($thr)*$L,sin($thr)*$L+$os1;
$L2=-1;
$L3=1;
$L4=3;
$L5=1;
printf "arrowhead %e %e %e %e\n",cos($thr)*$L2,sin($thr)*$L2+$os1,cos($thr)*$L3,sin($thr)*$L3+$os1;
printf "arc %e %e $os2 -90 0\n",cos($thr)*$L,sin($thr)*$L+$os1+$os2;
printf "#cm 1\narc %e %e $os2 0 90\n",cos($thr)*$L,sin($thr)*$L+$os1+$os2;
printf "arrowhead %e %e %e %e\n",cos($thr)*$L4,sin($thr)*$L4+$os1+$os2*2,cos($thr)*$L5,sin($thr)*$L5+$os1+$os2*2;
printf "fillcirc %e %e 0.1\n",cos($thr)*$L+$os2,sin($thr)*$L+$os1+$os2;
printf "%e %e \"B\"\n",cos($thr)*$L+$os2+0.5,sin($thr)*$L+$os1+$os2+0.3;
printf "line 0 %e %e %e\n",$os1+$os2*2,cos($thr)*$L,sin($thr)*$L+$os1+$os2*2;

printf "#cm 3\nfillcirc 0 %e 0.1\n",$os1+$os2*2;
printf "-0.5 %e \"C\"\n",$os1+$os2*2;
