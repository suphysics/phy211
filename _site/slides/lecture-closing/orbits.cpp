#include <math.h>      // use the system's math library
#include <stdio.h>     // use the system's text library
#include "vector.h"    // use my vector library


void animate(vector pos)
{
    printf("C 1 1 0\n");
    printf("c3 0 0 0 0.05\n");
    printf("C 0.5 0.5 1\n");
    printf("ct3 0 %e %e %e 0.03\n",pos.x,pos.y,pos.z);
    printf("F\n");
}

int main(void)         // all C programs require a "main" function
{
    double G = 4*M_PI*M_PI;         // G has a value 4*pi^2 if you measure distance in AU,
                                    // time in years, and mass in solar masses
				    
    double star_mass = 1;           // the Sun has a mass of one Sun worth
    double planet_mass = 0.001;     // the planet's mass is much smaller

    double radius;                  // we need a variable to hold the distance to the Sun

    vector position(1,0,0);  // put the planet off to the side in the x-direction
    vector velocity(0,3,0);  // throw the planet in the y-direction
    vector acceleration, force;

    double dt=0.0005;                // this is our "small amount of time"


    while(true)                     // Repeat the following forever:
    {
	position = position + velocity * dt; // kinematics update for position
	
        // The radius to the Sun is the magnitude of the position vector
	radius = magnitude(position);   

	// Use Newton's law of gravity to find the force
	force = (-direction(position)) * G * star_mass * planet_mass / (radius * radius);
	acceleration = force / planet_mass;  // Newton's 2nd law

	velocity = velocity + acceleration * dt; // kinematics update for velocity

	animate(position);
    }
}          

