---
layout: page
title: Calendar
permalink: calendar.html
category: top 
use_math: true
---

    
Note that the textbook does not track our presentation exactly; I've chosen the order of topics to simplify things for you, but that makes it a bit tougher to follow along in the book.

<br><br>

|-------------+------------------------------------------------------------------------------------------------------------------+--------------------------------+
| Class Date  | Topics                                                                                                           | Textbook sections              |
|:-----------:|:----------------------------------------------------------------------------------------------------------------:|:------------------------------:|
| 14 January  | <a href="slides/lecture-introduction/lecture-introduction.pdf">Introduction; what is physics?; measuring motion</a>      | 1.1-1.5                        |
| 16 January  | <a href="slides/lecture-calculus/lecture-calculus.pdf">Calculus: relating position, velocity, and acceleration</a>            | <a href="https://openstax.org/books/university-physics-volume-1/pages/3-1-position-displacement-and-average-velocity">Chapter 3</a>, especially <a href="https://openstax.org/books/university-physics-volume-1/pages/3-6-finding-velocity-and-displacement-from-acceleration">Sec. 3.6</a>  |
| 21 January  | <a href="slides/lecture-1D-motion/lecture-1D-motion.pdf">Analyzing motion in one dimension</a>|  | 
| 23 January  | <a href="slides/lecture-vectors/lecture-vectors.pdf">Vectors: mathematics with arrows</a>   |              <a href="https://openstax.org/books/university-physics-volume-1/pages/2-1-scalars-and-vectors">Chapter 2</a>, sections 1-3 (skip section 4 for now)                                |             
| 28 January  | <a href="slides/lecture-2D-motion/lecture-2D-motion.pdf">Kinematics in two dimensions</a> | <a href="https://openstax.org/books/university-physics-volume-1/pages/4-3-projectile-motion">Sec. 4.3</a>                              |
| 30 January  |<a href="slides/lecture-unit1-review/lecture-unit1-review.pdf">Unit 1 review</a>                                                                                                                |                                |
| 4  February | **Exam 1**                                                                                                       |                                |
| 6  February | <a href="slides/lecture-newtons-laws/lecture-newtons-laws.pdf">Newton's laws of motion</a>                                                                                                                |                                |
| 11 February | <a href="slides/lecture-friction/lecture-friction.pdf">Friction; forces in two dimensions</a>                                                                                                                |                                |
| 13 February | <a href="slides/lecture-circular-motion/lecture-circular-motion.pdf">Uniform circular motion</a>                                                                                                                   |                                |
| 18 February | <a href="slides/lecture-coupled-objects/lecture-coupled-objects.pdf">Coupled objects; traction; examples</a>                                                                                                                 |                                |
| 20 February | <a href="slides/lecture-exam2-review/lecture-exam2-review.pdf">Exam 2 review: last year's exam</a>                                                                                                                  |                                |
| 25 February | **Exam 2**                                                                                                       |                                |
| 27 February | <a href="slides/lecture-work-energy-theorem/lecture-work-energy-theorem.pdf">The work-energy theorem</a>                                                                                                                 |                                |
| 4  March    | <a href="slides/lecture-potential-energy/lecture-potential-energy.pdf">Potential energy</a>                                                                                                                |                                |
| 6  March    | <a href="slides/lecture-springs-power/lecture-springs-power.pdf">Elasticity and springs</a>                                                                                                                   |                                |
| 11+13 March | **SPRING BREAK**                                                                                                 |                                |
| 18 March    | Energy, power, and drag                                                                                                                  |                                |
| 20 March    | Impulse and momentum                                                                                                                  |                                |
| 25 March    | Momentum in combination                                                                                                                    |                                |
| 27 March    | Unit 3 Review                                                                                                                  |                                |
| 1  April    | **Exam 3**                                                                                                       |                                |
| 3  April    | Intro to rotational motion                                            |                                |
| 8  April    | Static equilibrium                   |                                |
| 10 April    | Rotational dynamics                                                                        |                                |
| 15 April    | Coupled rotation and translation                                                                                |                                |
| 17 April    | Rotational kinetic energy                                                                               |                                |
| 22 April    | Gear ratios; angular momentum                                                     |                                |
| 24 April    | Angular momentum in three dimensions; course closing and overview                                                                                  |                                |
| 5 May       | **FINAL EXAM**: 3PM-5PM Syracuse time                                                                            |                                |
|-------------+------------------------------------------------------------------------------------------------------------------+--------------------------------+

<br><br><br>

<img src="tutor-schedule.png" width="100%">


