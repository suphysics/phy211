---
layout: home
---

# Welcome to Physics 211!
Here you can find all of the information you'll need for PHY211.



### Today's Class Slides

* <a href="slides/lecture-springs-power/lecture-springs-power.pdf">Elasticity and springs</a>

### This Week's Recitation

* Week 8 Day 1: <a href="recitation/week8/recitation-potential-energy.pdf">More practice with the work-energy theorem; potential energy</a>
* Week 8 Day 2: <a href="recitation/week8/recitation-springs.pdf">The elastic force and elastic potential energy</a>

### Current Homework

* <a href="homework/hw5/hw5.pdf">Homework 5</a> is due Friday, March 7. No submissions will be accepted over email; if you are leaving for spring break early,
you should submit it to your TA before you leave.
 

### Exam 2 Study Materials

* Here is <a href="practice-exam-2-2025.pdf">Practice Exam 2</a> and its <a href="practice-exam-2-solutions.pdf">solutions</a>.
* Here's a link to <a href="slides/lecture-exam2-review/exam2-formA.pdf">last year's exam</a>, along with the solutions from the <a href="exam2-2024-930-solutions.pdf">9:30 class</a> and the solutions from the <a href="exam2-2024-1100-solutions.pdf">11:00 class</a>.
* Here are the <a href="https://youtu.be/md-ueG4jeMI">Homework 3 videos</a>, by Sydney, guest starring other aerospace engineering students
* Here are the <a href="https://www.youtube.com/playlist?list=PLfe92KWZipRNsDAjRsNMICtDWIzUPLhQV">Homework 4 videos</a>, by Walter
* Here are the videos for <a href="recitation-coupled-objects-solutions.pdf">the last recitation exercise you did</a>, by Sydney. (The others are on the "Videos and Solutions" page.)

                                                                                            
### Information

* Professor:
  * Walter Freeman, <wafreema@syr.edu>, Physics Building 215
* Recitation TA’s:
  * Adil Ghaznavi, <sghaznav@syr.edu>
  * Gourang Gehlot, <ggehlot@syr.edu>
  * Belal Menbari, <bmenbari@syr.edu>
  * Hanieh (Hani) Moradipasha, <hmoradip@syr.edu>
  * Srijana Pohkrel, <srpokhre@syr.edu>
  * Luis Rufino, <lerufino@syr.edu>
  * Jedidah (Jed) Tulu, <jetulu@syr.edu>
  * Javad Yousefian, <jyousefi@syr.edu>

* Class meeting: Tuesdays and Thursdays, 9:30-10:50 or 11:00-12:30, Tuesdays and Thursdays

* Course Discord server: <a href="https://discord.gg/SYuYbz44bp">[invite link]</a>


-  Group help sessions in the Physics Clinic:
  * Wednesdays 3-5 pm
  * Thursdays 1:30-3:30 pm
- Individual discussion hours - potentially available Tuesdays 4-5:30 and Wednesdays 9-12, sign up at <https://calendly.com/wfreeman-phy211/phy211-individual-discussion>



